-- Adminer 4.7.9 PostgreSQL dump

DROP TABLE IF EXISTS "categories";
DROP SEQUENCE IF EXISTS categories_id_seq;
CREATE SEQUENCE categories_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 START 1 CACHE 1;

CREATE TABLE "public"."categories" (
    "id" integer DEFAULT nextval('categories_id_seq') NOT NULL,
    "parent_id" integer,
    "order" integer DEFAULT '1' NOT NULL,
    "name" character varying(255) NOT NULL,
    "slug" character varying(255) NOT NULL,
    "created_at" timestamp(0),
    "updated_at" timestamp(0),
    CONSTRAINT "categories_pkey" PRIMARY KEY ("id"),
    CONSTRAINT "categories_slug_unique" UNIQUE ("slug")
) WITH (oids = false);


DROP TABLE IF EXISTS "data_rows";
DROP SEQUENCE IF EXISTS data_rows_id_seq;
CREATE SEQUENCE data_rows_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 START 1 CACHE 1;

CREATE TABLE "public"."data_rows" (
    "id" integer DEFAULT nextval('data_rows_id_seq') NOT NULL,
    "data_type_id" integer NOT NULL,
    "field" character varying(255) NOT NULL,
    "type" character varying(255) NOT NULL,
    "display_name" character varying(255) NOT NULL,
    "required" boolean DEFAULT false NOT NULL,
    "browse" boolean DEFAULT true NOT NULL,
    "read" boolean DEFAULT true NOT NULL,
    "edit" boolean DEFAULT true NOT NULL,
    "add" boolean DEFAULT true NOT NULL,
    "delete" boolean DEFAULT true NOT NULL,
    "details" text,
    "order" integer DEFAULT '1' NOT NULL,
    CONSTRAINT "data_rows_pkey" PRIMARY KEY ("id")
) WITH (oids = false);


DROP TABLE IF EXISTS "data_types";
DROP SEQUENCE IF EXISTS data_types_id_seq;
CREATE SEQUENCE data_types_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 START 1 CACHE 1;

CREATE TABLE "public"."data_types" (
    "id" integer DEFAULT nextval('data_types_id_seq') NOT NULL,
    "name" character varying(255) NOT NULL,
    "slug" character varying(255) NOT NULL,
    "display_name_singular" character varying(255) NOT NULL,
    "display_name_plural" character varying(255) NOT NULL,
    "icon" character varying(255),
    "model_name" character varying(255),
    "description" character varying(255),
    "generate_permissions" boolean DEFAULT false NOT NULL,
    "created_at" timestamp(0),
    "updated_at" timestamp(0),
    "server_side" smallint DEFAULT '0' NOT NULL,
    "controller" character varying(255),
    "policy_name" character varying(255),
    "details" text,
    CONSTRAINT "data_types_name_unique" UNIQUE ("name"),
    CONSTRAINT "data_types_pkey" PRIMARY KEY ("id"),
    CONSTRAINT "data_types_slug_unique" UNIQUE ("slug")
) WITH (oids = false);


DROP TABLE IF EXISTS "depenses";
DROP SEQUENCE IF EXISTS depenses_id_seq;
CREATE SEQUENCE depenses_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1;

CREATE TABLE "public"."depenses" (
    "id" bigint DEFAULT nextval('depenses_id_seq') NOT NULL,
    "id_user" integer,
    "date_depenses" date NOT NULL,
    "montant" double precision NOT NULL,
    "justificatif" character varying(255) NOT NULL,
    "remarques" text,
    "created_at" timestamp(0),
    "updated_at" timestamp(0),
    CONSTRAINT "depenses_pkey" PRIMARY KEY ("id")
) WITH (oids = false);


DROP TABLE IF EXISTS "failed_jobs";
DROP SEQUENCE IF EXISTS failed_jobs_id_seq;
CREATE SEQUENCE failed_jobs_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1;

CREATE TABLE "public"."failed_jobs" (
    "id" bigint DEFAULT nextval('failed_jobs_id_seq') NOT NULL,
    "uuid" character varying(255) NOT NULL,
    "connection" text NOT NULL,
    "queue" text NOT NULL,
    "payload" text NOT NULL,
    "exception" text NOT NULL,
    "failed_at" timestamp(0) DEFAULT CURRENT_TIMESTAMP NOT NULL,
    CONSTRAINT "failed_jobs_pkey" PRIMARY KEY ("id"),
    CONSTRAINT "failed_jobs_uuid_unique" UNIQUE ("uuid")
) WITH (oids = false);


DROP TABLE IF EXISTS "file_uploads";
DROP SEQUENCE IF EXISTS file_uploads_id_seq;
CREATE SEQUENCE file_uploads_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1;

CREATE TABLE "public"."file_uploads" (
    "id" bigint DEFAULT nextval('file_uploads_id_seq') NOT NULL,
    "name" character varying(255),
    "path" character varying(255),
    "created_at" timestamp(0),
    "updated_at" timestamp(0),
    CONSTRAINT "file_uploads_pkey" PRIMARY KEY ("id")
) WITH (oids = false);


DROP TABLE IF EXISTS "menu_items";
DROP SEQUENCE IF EXISTS menu_items_id_seq;
CREATE SEQUENCE menu_items_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 START 1 CACHE 1;

CREATE TABLE "public"."menu_items" (
    "id" integer DEFAULT nextval('menu_items_id_seq') NOT NULL,
    "menu_id" integer,
    "title" character varying(255) NOT NULL,
    "url" character varying(255) NOT NULL,
    "target" character varying(255) DEFAULT '_self' NOT NULL,
    "icon_class" character varying(255),
    "color" character varying(255),
    "parent_id" integer,
    "order" integer NOT NULL,
    "created_at" timestamp(0),
    "updated_at" timestamp(0),
    "route" character varying(255),
    "parameters" text,
    CONSTRAINT "menu_items_pkey" PRIMARY KEY ("id")
) WITH (oids = false);


DROP TABLE IF EXISTS "menus";
DROP SEQUENCE IF EXISTS menus_id_seq;
CREATE SEQUENCE menus_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 START 1 CACHE 1;

CREATE TABLE "public"."menus" (
    "id" integer DEFAULT nextval('menus_id_seq') NOT NULL,
    "name" character varying(255) NOT NULL,
    "created_at" timestamp(0),
    "updated_at" timestamp(0),
    CONSTRAINT "menus_name_unique" UNIQUE ("name"),
    CONSTRAINT "menus_pkey" PRIMARY KEY ("id")
) WITH (oids = false);


DROP TABLE IF EXISTS "migrations";
DROP SEQUENCE IF EXISTS migrations_id_seq;
CREATE SEQUENCE migrations_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 START 1 CACHE 1;

CREATE TABLE "public"."migrations" (
    "id" integer DEFAULT nextval('migrations_id_seq') NOT NULL,
    "migration" character varying(255) NOT NULL,
    "batch" integer NOT NULL,
    CONSTRAINT "migrations_pkey" PRIMARY KEY ("id")
) WITH (oids = false);


DROP TABLE IF EXISTS "offrandes";
DROP SEQUENCE IF EXISTS offrandes_id_seq;
CREATE SEQUENCE offrandes_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1;

CREATE TABLE "public"."offrandes" (
    "id" bigint DEFAULT nextval('offrandes_id_seq') NOT NULL,
    "id_user" integer,
    "date_offrandes" date NOT NULL,
    "montant" double precision NOT NULL,
    "remarques" text,
    "created_at" timestamp(0),
    "updated_at" timestamp(0),
    CONSTRAINT "offrandes_pkey" PRIMARY KEY ("id")
) WITH (oids = false);


DROP TABLE IF EXISTS "pages";
DROP SEQUENCE IF EXISTS pages_id_seq;
CREATE SEQUENCE pages_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 START 1 CACHE 1;

CREATE TABLE "public"."pages" (
    "id" integer DEFAULT nextval('pages_id_seq') NOT NULL,
    "author_id" integer NOT NULL,
    "title" character varying(255) NOT NULL,
    "excerpt" text,
    "body" text,
    "image" character varying(255),
    "slug" character varying(255) NOT NULL,
    "meta_description" text,
    "meta_keywords" text,
    "status" character varying(255) DEFAULT 'INACTIVE' NOT NULL,
    "created_at" timestamp(0),
    "updated_at" timestamp(0),
    CONSTRAINT "pages_pkey" PRIMARY KEY ("id"),
    CONSTRAINT "pages_slug_unique" UNIQUE ("slug")
) WITH (oids = false);


DROP TABLE IF EXISTS "password_resets";
CREATE TABLE "public"."password_resets" (
    "email" character varying(255) NOT NULL,
    "token" character varying(255) NOT NULL,
    "created_at" timestamp(0)
) WITH (oids = false);

CREATE INDEX "password_resets_email_index" ON "public"."password_resets" USING btree ("email");


DROP TABLE IF EXISTS "permission_role";
CREATE TABLE "public"."permission_role" (
    "permission_id" bigint NOT NULL,
    "role_id" bigint NOT NULL,
    CONSTRAINT "permission_role_pkey" PRIMARY KEY ("permission_id", "role_id")
) WITH (oids = false);

CREATE INDEX "permission_role_permission_id_index" ON "public"."permission_role" USING btree ("permission_id");

CREATE INDEX "permission_role_role_id_index" ON "public"."permission_role" USING btree ("role_id");


DROP TABLE IF EXISTS "permissions";
DROP SEQUENCE IF EXISTS permissions_id_seq;
CREATE SEQUENCE permissions_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1;

CREATE TABLE "public"."permissions" (
    "id" bigint DEFAULT nextval('permissions_id_seq') NOT NULL,
    "key" character varying(255) NOT NULL,
    "table_name" character varying(255),
    "created_at" timestamp(0),
    "updated_at" timestamp(0),
    CONSTRAINT "permissions_pkey" PRIMARY KEY ("id")
) WITH (oids = false);

CREATE INDEX "permissions_key_index" ON "public"."permissions" USING btree ("key");


DROP TABLE IF EXISTS "posts";
DROP SEQUENCE IF EXISTS posts_id_seq;
CREATE SEQUENCE posts_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 START 1 CACHE 1;

CREATE TABLE "public"."posts" (
    "id" integer DEFAULT nextval('posts_id_seq') NOT NULL,
    "author_id" integer NOT NULL,
    "category_id" integer,
    "title" character varying(255) NOT NULL,
    "seo_title" character varying(255),
    "excerpt" text,
    "body" text NOT NULL,
    "image" character varying(255),
    "slug" character varying(255) NOT NULL,
    "meta_description" text,
    "meta_keywords" text,
    "status" character varying(255) DEFAULT 'DRAFT' NOT NULL,
    "featured" boolean DEFAULT false NOT NULL,
    "created_at" timestamp(0),
    "updated_at" timestamp(0),
    CONSTRAINT "posts_pkey" PRIMARY KEY ("id"),
    CONSTRAINT "posts_slug_unique" UNIQUE ("slug")
) WITH (oids = false);


DROP TABLE IF EXISTS "roles";
DROP SEQUENCE IF EXISTS roles_id_seq;
CREATE SEQUENCE roles_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1;

CREATE TABLE "public"."roles" (
    "id" bigint DEFAULT nextval('roles_id_seq') NOT NULL,
    "name" character varying(255) NOT NULL,
    "display_name" character varying(255) NOT NULL,
    "created_at" timestamp(0),
    "updated_at" timestamp(0),
    CONSTRAINT "roles_name_unique" UNIQUE ("name"),
    CONSTRAINT "roles_pkey" PRIMARY KEY ("id")
) WITH (oids = false);


DROP TABLE IF EXISTS "settings";
DROP SEQUENCE IF EXISTS settings_id_seq;
CREATE SEQUENCE settings_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 START 1 CACHE 1;

CREATE TABLE "public"."settings" (
    "id" integer DEFAULT nextval('settings_id_seq') NOT NULL,
    "key" character varying(255) NOT NULL,
    "display_name" character varying(255) NOT NULL,
    "value" text,
    "details" text,
    "type" character varying(255) NOT NULL,
    "order" integer DEFAULT '1' NOT NULL,
    "group" character varying(255),
    CONSTRAINT "settings_key_unique" UNIQUE ("key"),
    CONSTRAINT "settings_pkey" PRIMARY KEY ("id")
) WITH (oids = false);


DROP TABLE IF EXISTS "translations";
DROP SEQUENCE IF EXISTS translations_id_seq;
CREATE SEQUENCE translations_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 START 1 CACHE 1;

CREATE TABLE "public"."translations" (
    "id" integer DEFAULT nextval('translations_id_seq') NOT NULL,
    "table_name" character varying(255) NOT NULL,
    "column_name" character varying(255) NOT NULL,
    "foreign_key" integer NOT NULL,
    "locale" character varying(255) NOT NULL,
    "value" text NOT NULL,
    "created_at" timestamp(0),
    "updated_at" timestamp(0),
    CONSTRAINT "translations_pkey" PRIMARY KEY ("id"),
    CONSTRAINT "translations_table_name_column_name_foreign_key_locale_unique" UNIQUE ("table_name", "column_name", "foreign_key", "locale")
) WITH (oids = false);


DROP TABLE IF EXISTS "user_roles";
CREATE TABLE "public"."user_roles" (
    "user_id" bigint NOT NULL,
    "role_id" bigint NOT NULL,
    CONSTRAINT "user_roles_pkey" PRIMARY KEY ("user_id", "role_id")
) WITH (oids = false);

CREATE INDEX "user_roles_role_id_index" ON "public"."user_roles" USING btree ("role_id");

CREATE INDEX "user_roles_user_id_index" ON "public"."user_roles" USING btree ("user_id");


DROP TABLE IF EXISTS "users";
DROP SEQUENCE IF EXISTS users_id_seq;
CREATE SEQUENCE users_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1;

CREATE TABLE "public"."users" (
    "id" bigint DEFAULT nextval('users_id_seq') NOT NULL,
    "name" character varying(255) NOT NULL,
    "email" character varying(255) NOT NULL,
    "email_verified_at" timestamp(0),
    "password" character varying(255) NOT NULL,
    "remember_token" character varying(100),
    "created_at" timestamp(0),
    "updated_at" timestamp(0),
    "avatar" character varying(255) DEFAULT 'users/default.png',
    "role_id" bigint,
    "settings" text,
    "api_token" character varying(80),
    CONSTRAINT "users_api_token_unique" UNIQUE ("api_token"),
    CONSTRAINT "users_email_unique" UNIQUE ("email"),
    CONSTRAINT "users_pkey" PRIMARY KEY ("id")
) WITH (oids = false);

INSERT INTO "users" ("id", "name", "email", "email_verified_at", "password", "remember_token", "created_at", "updated_at", "avatar", "role_id", "settings", "api_token") VALUES
(2,	'marubo',	'sahibmarubo@gmail.com',	NULL,	'$2y$10$jWvXnJhzMegasKRDTkBGm.3Ju/J.cuFYxnEuHu5wf81HBX2Mf9Kka',	NULL,	'2022-07-07 20:38:35',	'2022-07-07 20:39:49',	'users/default.png',	1,	NULL,	'I68dgtlECRSaQPAw0nfJY2NngN1Iqsx6NmDq5PSNZS7axfHtooHvN8x5s8h1'),
(3,	'sahib',	'sahibmartial@gmail.com',	NULL,	'$2y$10$kKaxzOfjT4Rj7M.akyOWGuSb81UDn.4nGrSDi77/UraxnVlwdHWTe',	NULL,	'2022-07-07 20:45:00',	'2022-07-07 20:45:00',	'users/July2022/GJdxymxu1TaOlrjYWu5i.jpg',	3,	'{"locale":"fr"}',	NULL),
(4,	'marubo',	'formateurmarubo@gmail.com',	NULL,	'$2y$10$OISPmcnsf9UjScdFuypkx.l0/Pt16e3/3r.pz2lQLwxjlrJMByG/O',	NULL,	'2022-07-09 12:11:59',	'2022-07-09 12:14:02',	'users/default.png',	5,	'{"locale":"fr"}',	NULL);

ALTER TABLE ONLY "public"."categories" ADD CONSTRAINT "categories_parent_id_foreign" FOREIGN KEY (parent_id) REFERENCES categories(id) ON UPDATE CASCADE ON DELETE SET NULL NOT DEFERRABLE;

ALTER TABLE ONLY "public"."data_rows" ADD CONSTRAINT "data_rows_data_type_id_foreign" FOREIGN KEY (data_type_id) REFERENCES data_types(id) ON UPDATE CASCADE ON DELETE CASCADE NOT DEFERRABLE;

ALTER TABLE ONLY "public"."depenses" ADD CONSTRAINT "depenses_id_user_foreign" FOREIGN KEY (id_user) REFERENCES users(id) ON UPDATE CASCADE ON DELETE SET NULL NOT DEFERRABLE;

ALTER TABLE ONLY "public"."menu_items" ADD CONSTRAINT "menu_items_menu_id_foreign" FOREIGN KEY (menu_id) REFERENCES menus(id) ON DELETE CASCADE NOT DEFERRABLE;

ALTER TABLE ONLY "public"."offrandes" ADD CONSTRAINT "offrandes_id_user_foreign" FOREIGN KEY (id_user) REFERENCES users(id) ON UPDATE CASCADE ON DELETE SET NULL NOT DEFERRABLE;

ALTER TABLE ONLY "public"."permission_role" ADD CONSTRAINT "permission_role_permission_id_foreign" FOREIGN KEY (permission_id) REFERENCES permissions(id) ON DELETE CASCADE NOT DEFERRABLE;
ALTER TABLE ONLY "public"."permission_role" ADD CONSTRAINT "permission_role_role_id_foreign" FOREIGN KEY (role_id) REFERENCES roles(id) ON DELETE CASCADE NOT DEFERRABLE;

ALTER TABLE ONLY "public"."user_roles" ADD CONSTRAINT "user_roles_role_id_foreign" FOREIGN KEY (role_id) REFERENCES roles(id) ON DELETE CASCADE NOT DEFERRABLE;
ALTER TABLE ONLY "public"."user_roles" ADD CONSTRAINT "user_roles_user_id_foreign" FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE NOT DEFERRABLE;

ALTER TABLE ONLY "public"."users" ADD CONSTRAINT "users_role_id_foreign" FOREIGN KEY (role_id) REFERENCES roles(id) NOT DEFERRABLE;

-- 2022-07-18 22:40:56.608512+02
