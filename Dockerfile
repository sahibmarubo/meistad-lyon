# Set the base image for subsequent instructions
FROM php:8.0-fpm

# Arguments defined in docker-compose.yml
ARG user
ARG uid

# Update packages
RUN apt-get update

# Install dependencies
RUN apt-get update && apt-get install -y \
   libpng-dev \
   libonig-dev \
   libxml2-dev \
   libpq-dev\
   zip \
   vim \
   unzip \
   git \
   curl\
   nano
# Install PHP and composer dependencies
#RUN apt-get install -qq git curl libmcrypt-dev libjpeg-dev libpng-dev libfreetype6-dev libbz2-dev

# Clear out the local repository of retrieved package files
#RUN apt-get clean
RUN apt-get clean && rm -rf /var/lib/apt/lists/*

# Install extensions
RUN docker-php-ext-configure pgsql -with-pgsql=/usr/local/pgsql

RUN docker-php-ext-install pdo pdo_pgsql pgsql mbstring exif pcntl bcmath gd

# Install needed extensions
# Here you can install any other extension that you need during the test and deployment process
#RUN docker-php-ext-configure gd --with-freetype --with-jpeg
#RUN docker-php-ext-install -j$(nproc) gd
#RUN docker-php-ext-install pdo_mysql zip

# Install Composer
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer
#RUN curl --silent --show-error "https://getcomposer.org/installer" | php -- --install-dir=/usr/local/bin --filename=composer


# Create system user to run Composer and Artisan Commands
RUN useradd -G www-data,root -u $uid -d /home/$user $user
RUN mkdir -p /home/$user/.composer && \
    chown -R $user:$user /home/$user

COPY composer.lock composer.json /var/www/


# Set working directory
WORKDIR /var/www

# Install Laravel Envoy
#RUN composer global require "laravel/envoy=~1.0"
# Install Cron
RUN apt-get update
RUN apt-get -y install cron
# Add the cron job
RUN crontab -l  | { cat; echo "* * * * * cd /var/wwww && php artisan schedule:run >> test.log"; } | crontab -
#RUN crontab -l  | { cat; echo "* * * 01-12 * cd /var/wwww && php artisan MailAlertImpot:send >> test.log"; } | crontab -

#CMD ["cron", "-f"]
# Run the command on conatainer startup

#CMD [ "cd /var/wwww && php artisan config:clear" ]
#CMD  * * * 01-12 * cd /var/wwww && php artisan MailAlertImpot:send >> test.log
# Copy composer.lock and composer.json
#CMD bash -c "composer install && chmod -R 777 /var/www && php artisan migrate --seed && php artisan storage:link"
USER $user

