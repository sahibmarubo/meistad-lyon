<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Depenses extends Model
{
    use HasFactory;

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $fillable = [
        'id_user',
        'date_depenses',
        'montant',
        'justificatif',
        'remarques',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime:Y-m-d ',
        'updated_at' => 'datetime:Y-m-d ',

    ];

    /**
     * Retourne depenses by Id
     *
     * @param  int  $id
     * @return $result
     */
    public function getDepenseByid($id)
    {
        try {
            $result = DB::table('depenses')
            ->join('users','depenses.id_user','=','users.id')
            ->where('depenses.id',$id)
            ->get(['depenses.id','date_depenses','name','montant','remarques'])->first();
            //$result = Depenses::whereId($id)->first();
        } catch (\Exception $e) {
            return $e->getMessage();
        }
        if ($result) {
            return  $result;
        }
    }

    /**
     * Get list of depenses with year
     *
     * @param  date  $year
     * @return $results
     */
    public function searchDepenseByYear($year)
    {
        try {
            $results = DB::table('depenses')
                ->whereYear('date_depenses', '=', $year)
                ->orderByDesc('id')->get();
        } catch (\Exception $e) {
            return $e->getMessage();
        }

        return  $results;
    }

    /**
     * SearchDepenseByMonth
     *
     * @param  date  $year
     * @param  date  $month
     * @return $results
     */
    public function searchDepenseByMonth($year, $month)
    {
        try {
            $results = DB::table('depenses')
                ->whereYear('date_depenses', '=', $year)
                ->whereMonth('date_depenses', '=', $month)
                ->orderByDesc('id')->get();
        } catch (\Exception $e) {
            return $e->getMessage();
        }

        return  $results;
    }
}
