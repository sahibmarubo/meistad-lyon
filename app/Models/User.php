<?php

namespace App\Models;

use Filament\Models\Contracts\FilamentUser;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User  extends Authenticatable implements FilamentUser, MustVerifyEmail
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'phone',
        'password',
        'api_token',
        'role_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime:Y-m-d ',
        'created_at' => 'datetime:Y-m-d ',
        'updated_at' => 'datetime:Y-m-d ',

    ];

    /**
     * GetUserPasteur
     *
     * @return array $useralertEmail
     */
    public function getEmailUserPasteurAndRespo()
    {
        $result = [];

        try {
            $useralertEmail = User::whereIN('role_id', [5,6,3]) //user respo, pasteur and tresorier
            ->get('email')->toArray();
        } catch (\Exception $e) {
            return $e->getMessage();
        }

        if (! empty($useralertEmail)) {
            foreach ($useralertEmail as $key => $value) {
                //dd($value['email']) ;
                $result[] = $value['email'];
            }
        }

        return $result;
    }
    /**
     * Summary of canAccessPanel
     * @param \Filament\Panel $panel
     * @return bool
     */
    public function canAccessPanel(\Filament\Panel $panel): bool
    {
        $roles = Role::pluck('id', 'name');
       
        if ($panel->getId() === 'admin') {
             
            return str_ends_with($this->email, 'sahibmartial@gmail.com') && $this->hasVerifiedEmail();
            //return str_ends_with($this->role_id, $roles['super-admin']) && $this->hasVerifiedEmail();
        } 
        return abort(403, '⛔ Unauthorized, thanks! ⛔'); // add message not unauthorized
    }

   


}
