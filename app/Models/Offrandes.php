<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Offrandes extends Model
{
    use HasFactory;

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $fillable = [
        'id_user',
        'date_offrandes',
        'montant',
        'justificatif',
        'remarques',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime:Y-m-d ',
        'updated_at' => 'datetime:Y-m-d ',

    ];

    /**
     * get offrandes this month
     */
    public function getOffrandesOfThisMonth($mois)
    {
    }

    /**
     * get offrandes by id
     */
    public function getOffrandesByid($id)
    {
        try {
            $result = DB::table('offrandes')
            ->join('users','offrandes.id_user','=','users.id')
            ->where('offrandes.id',$id)
            ->get(['offrandes.id','date_offrandes','name','montant','remarques'])->first();
            //$result = Offrandes::whereId($id)->first();


        } catch (\Exception $e) {
            return $e->getMessage();
        }

        return $result;
    }

    /**
     * get offrandes by year
     */
    public function SearchOffrandesByYear($year)
    {
        try {
            $results = DB::table('offrandes')
        ->whereYear('date_offrandes', '=', $year)
        ->orderBy('id','desc')
        ->get();
        } catch (\Exception $e) {
            return  $e->getMessage();

        }


        return  $results;
    }

    public function searchOffrandesByMonth($year, $month)
    {
        try {
            $results = DB::table('offrandes')
                ->whereYear('date_offrandes', '=', $year)
                ->whereMonth('date_offrandes', '=', $month)
                ->orderByDesc('id')->get();
        } catch (\Exception $e) {
            return $e->getMessage();
        }

        return  $results;

    }
}
