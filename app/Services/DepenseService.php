<?php
namespace App\Services;

use DB;

class DepenseService
{

  public function getListingYearDepense()
  {
    try {
        $result= DB::select("
        SELECT EXTRACT(year from date_depenses) as year
        FROM depenses
        GROUP BY year
        ");
    } catch (\Exception $e) {
        //throw $th;
        return $e->getMessage();
    }

    return $result;
  }
}
