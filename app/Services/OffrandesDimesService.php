<?php
namespace App\Services;

use DB;

class OffrandesDimesService
{
    public function getListYearOffrandes()
    {
        try {
            $result= DB::select("
            SELECT EXTRACT(year from date_offrandes) as year
            FROM offrandes
            GROUP BY year
            ");
        } catch (\Exception $e) {
            //throw $th;
            return $e->getMessage();
        }

        return $result;

    }
}
