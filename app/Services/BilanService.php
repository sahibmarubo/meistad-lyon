<?php
namespace App\Services;

use App\Models\Offrandes;
use DB;

class BilanService
{


    public function statCaisse()
    {

        try {
            $offrandesDimes = DB::select("SELECT
            EXTRACT(year FROM date_offrandes) as year,
            SUM(montant) as offrande_dimes

            FROM offrandes
            GROUP BY EXTRACT(year FROM date_offrandes)");

            $depenses= DB::select("SELECT
            EXTRACT(year FROM date_depenses) as year,
            SUM(montant) as depense

            FROM depenses
            GROUP BY EXTRACT(year FROM date_depenses)");
        } catch (\Exception $e) {
            //throw $th;
            return $e->getMessage();
        }

        $years=[];
        $bilans=[];
        //Traitement caisse par année
        foreach ($offrandesDimes as $key => $od) {
            foreach ($depenses as $key => $depense) {
                if($od->year==$depense->year){

                    $bilan=round((round($od->offrande_dimes,2)-round($depense->depense,2)),2);

                    array_push($years,$od->year);

                    array_push($bilans,$bilan);
                }
            }
        }

        return ['Year'=>$years,'Bilan'=>$bilans];

    }

    public function listYear()
    {
        try {
            $result= DB::select("
            SELECT EXTRACT(year from date_offrandes) as year
            FROM offrandes
            GROUP BY year
            ");
        } catch (\Exception $e) {
            //throw $th;
            return $e->getMessage();
        }

        return $result;

    }
}
