<?php

namespace App\Exceptions;

use BadMethodCallException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Support\Facades\Log;
use InvalidArgumentException;
use LogicException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Mailer\Exception\TransportException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
          
            if ($e instanceof NotFoundHttpException) {
                Log::info('From renderable method: '.$e->getMessage());
                //return response('Http error',400);
                abort(400, '🚨 Oups Http error, thanks 🚨');
            }
            if ($e instanceof TransportException) {
                Log::info('From renderable method: '.$e->getMessage());
                //return response('Http error',400);
                abort(500, '🚨 Oups Http error, thanks 🚨');
            }
            if ($e instanceof BadMethodCallException) {
                Log::info('From renderable method: '.$e->getMessage());
                //return response('Bad medthode exception',500);
                abort(500, '🚨 Bad medthode exception 🚨');
            }
            if ($e instanceof LogicException) {
                Log::info('From renderable method: '.$e->getMessage());
                //return response('Error logic exception',500);
                abort(500, '🚨 Error logic exception  🚨');

            }
            if ($e instanceof MethodNotAllowedHttpException) {
                Log::info('From renderable method: '.$e->getMessage());
                //return response('🚨 Oups Something wrong, thanks 🚨',404);
                abort(404, '🚨 Oups Something wrong, thanks 🚨');

            }
            if ($e instanceof InvalidArgumentException) {
                Log::info('From renderable method: '.$e->getMessage());
                abort(404, '🚨 Oups Something wrong, thanks 🚨');
                //return response('🚨 Oups Something wrong, thanks 🚨',404);

            }
           
        });
    }
}
