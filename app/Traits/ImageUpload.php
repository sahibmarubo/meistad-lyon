<?php

namespace App\Traits;

use App\Models\FileUpload;
use Illuminate\Http\Request;

trait ImageUpload
{
    /**
     * Summary of upload
     * @param \Illuminate\Http\Request $request
     * @param mixed $fieldname
     * @param mixed $directory
     * @return string
     */
    public static function upload(Request $request, $fieldname = 'image', $directory = 'uploads' ) 
    {
        $fileUpload = new FileUpload;
        //rename file 
        $file_name = time().'.'.$request->file->getClientOriginalExtension();
        //$request->file->getClientOriginalName();
        //storage 
        $file_path = $request->file('file')->storeAs('uploads', $file_name, 'public');
        //$fileUpload->name = time().'_'.$request->file->getClientOriginalName();
        $fileUpload->path = '/storage/'.$file_path;
       
        return $file_name;

    }

}
