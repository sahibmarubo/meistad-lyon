<?php

namespace App\Livewire;

use App\Models\Event;
use Livewire\Component;

class CalendarPulic extends Component
{
    public $events = [];
    public function render()
    {
        
        $this->events = json_encode(Event::whereCible('public')->get());
       
        return view('livewire.calendar-pulic');
    }
}
