<?php

namespace App\Livewire;

use App\Models\Event;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Livewire\Component;
use Validator;

class Calendar extends Component
{
    public $user;
    public $title;
    public $alertMessage = 'alert alert-info';
    public $events = [];
    public function eventChange($event)
    {
        $e = Event::find($event['id']);
        $e->start = $event['start'];
        if(Arr::exists($event, 'end')) {
            $e->end = $event['end'];
        }
        $e->save();
    }
    public function eventAdd($event)
    {
       
        $validated = Validator::make(
            [
                'title' => $event['title']
            ],
            [
                'title' => 'required|string|min:3|regex:/^[a-zA-Z-,-: \' \p{L}-]*$/u',
            ]
        )->validate();
         
        $myEvent = Arr::except($event,['allDay']);
      
      
       Event::create($myEvent);
    }
    public function eventRemove($id)
    {
        Event::destroy($id);
    }
    public function render()
    {
      
        $this->events = json_encode(Event::all());
       
        return view('livewire.calendar');
    }
}
