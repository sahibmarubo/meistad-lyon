<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Storage;

class DumpDatabase extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'DumpDatabase:weekly';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command dumpdatabase weekly';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $backupOfWeekendFound=false;
        //application name
        $myApp=env('APP_NAME');
        // ath to move dump db
        $path=env('APP_ENV')!='local'?env('SOURCE_BACKUP'):env('SOURCE_BACKUP_PPOD');
        //currentDate
        $date=date('d-m-Y');
        //extension file dump
        $output="output.sql";
        //dump generate name
        $DumpDatabase=$date."_".$myApp."-".$output;
        //zip dump file 
        $filename = $path.$DumpDatabase. ".gz";
        //check directory not assume that directory created before

        if (!Storage::disk('dumpdatabase')->files()) {
            $this->info("Process Dump database  firstly today");
            //genarate command dump file and mv to path
             $this->DumpDatabase($filename);
            Log::info('Dump database successfully '.$myApp.' '.$date);

        }else{
            $this->info("Process Dump database today and clean old file dump");
            //genarate command dump file and mv to path
            $this->DumpDatabase($filename);
            Log::info('Dump database successfully '.$myApp.' '.$date);
            //process to clean old file 
            //check dump today existe before  launch process remove old dump file 
            if (Storage::disk('dumpdatabase')->exists($DumpDatabase.'.gz')) { 
                $backupOfWeekendFound=$this->checkDumpFileWeekendFounded();
            }
            //Step Clean old file 
            if($backupOfWeekendFound==true){
                $this->deleteOldDumpFile();
                Log::info('Clean old Dump database successfully '.$myApp.' '.$date);
                $this->info('Clean old Dump database successfully '.$myApp.' '.$date);
            }
        }
       
    }
    /**
     * Summary of DumpDatabase
     * @param string $filename
     * @return void
     */
    public function DumpDatabase(string $filename)
    { 
        
        //dump database and mv to filename
        $command ='php artisan db:masked-dump '.$filename ;
        $returnVar = NULL;
        $output  = NULL;
        //Execute dump file 
        exec($command, $output, $returnVar);
        
    }
    /**
     * Summary of checkDumpFileWeekendFounded
     * @return bool
     */
    public function checkDumpFileWeekendFounded()
    {
        foreach (Storage::disk('dumpdatabase')->files() as $key => $file) {
            //Step Search backup of this weekeed 
            $segments = Str::of($file)->explode('_');
            if(isset($segments[0])){
                if($segments[0]==date('d-m-Y')){
                    return true;
                }
            }
        }
        return false;

    }
    /**
     * Summary of deleteOldDumpFile
     * @return void
     */
    public function deleteOldDumpFile()
    {
        foreach (Storage::disk('dumpdatabase')->files() as $key => $deleteFile) {
            $fileTodelete=Str::of($deleteFile)->explode('_');
            if($fileTodelete[0]!=date('d-m-Y')){
                echo($deleteFile).PHP_EOL;
                Storage::disk('dumpdatabase')->delete($deleteFile);
            }
        }

    }
}
