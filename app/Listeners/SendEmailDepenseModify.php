<?php

namespace App\Listeners;

use App\Event\DepenseModify;
use App\Http\Controllers\MailController;
use App\Models\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendEmailDepenseModify
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Event\DepenseModify  $event
     * @return void
     */
    public function handle(DepenseModify $event)
    {
        $users = new User();
        $mail = new MailController;

        $emails=$users->getEmailUserPasteurAndRespo();

        $subject = '📢 Modification Depenses';
        $content = 'Bonjour, modification depense n°'.$event->depense['Id'].' d\'un nouveau montant de '.$event->depense['Montant'].' € le '.$event->depense['Date'].'👍<br>.';
        $content .='Montant précedent '.$event->depense['Montant_prev']. ' €. <br/>';

        foreach ($emails as $key => $value) {

            $mail->sendEmailFidele($value, $subject, $content);
        }


    }
}
