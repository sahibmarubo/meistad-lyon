<?php

namespace App\Listeners;

use App\Events\SendMessage;
use App\Http\Controllers\MailController;
use App\Models\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Facades\Vonage;
use Illuminate\Queue\InteractsWithQueue;
use Vonage\Client;
use Vonage\Client\Credentials;
class SendMessagesNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\SendMessage  $event
     * @return void
     */
    public function handle(SendMessage $event)
    {


        $mail = new MailController;
        $subject ='📢 '.$event->body['Data']->subject;

        $content = $event->body['Data']->greeting.PHP_EOL;
        $content .=$event->body['Data']->introLines[0].PHP_EOL;
        $content .=$event->body['Data']->outroLines[0].PHP_EOL;
        //dd($content);
        //Choissir option mail or sms
        if($event->body['type']=='EMAIL') {

           if($event->body['Desti']) {

                foreach ($event->body['Desti'] as $key => $value) {

                    $mail->sendEmailFidele($value, $subject, $content);
                }
            }else {
                $users=User::all('email')->toArray();

                foreach ($users as $key => $value) {

                    $mail->sendEmailFidele($value['email'], $subject, $content);
                }
            }

        }elseif($event->body['type']=='SMS') {

            //$client =  new Vonage\Client(new Vonage\Client\Credentials\Basic(env('VONAGE_KEY'),env('VONAGE_SECRET_KEY')));
             //dd($client);
            $message=$event->body['Data']->introLines[0];

             $message.=" ";
            $message.=$event->body['Data']->outroLines[0];
            //dd($message);
            //$message='A text message sent using the Nexmo SMS API';
            $sujet='MEISTAD-LYON';

            if($event->body['Desti']) {

                foreach ($event->body['Desti'] as $key => $value) {
                    $sms='curl -X "POST" "https://rest.nexmo.com/sms/json" \
                    -d "from=Meistad LYON" \
                    -d "text='.$message.'" \
                    -d "to='.$value.'" \
                    -d "api_key='.env('VONAGE_KEY').'" \
                    -d "api_secret='.env('VONAGE_SECRET_KEY').'"';

                    //dd($sms);
                    shell_exec($sms);
                }

            }else {
                $users=User::all('phone')->toArray();

                foreach ($event->body['Desti'] as $key => $value) {
                    $sms='curl -X "POST" "https://rest.nexmo.com/sms/json" \
                    -d "from=Meistad LYON" \
                    -d "text='.$message.'" \
                    -d "to='.$value.'" \
                    -d "api_key='.env('VONAGE_KEY').'" \
                    -d "api_secret='.env('VONAGE_SECRET_KEY').'"';

                    //dd($sms);
                    shell_exec($sms);
                }



            }


           // $basic  = new \Vonage\Client\Credentials\Basic("23567dd7", "8trHKBi4R62dtPRi");
            //$client = new \Vonage\Client($basic);

            $user="'23567dd7:8trHKBi4R62dtPRi'";
            $header1="'Content-Type: application/json'";
            $header2="'Accept: application/json'";
            $separator=' \\ ';
            $TO_NUMBER=' "33769221935"';
            $message=' "This is a WhatsApp Message sent from the Messages API"';

            /*$content='curl -X POST https://messages-sandbox.nexmo.com/v1/messages'.$separator.
            '-u '.$user.$separator.
            '-H '.$header1.$separator.
            '-H '.$header2.$separator.
            '-d \''.'{ "from": "14157386102",
                "to":'.$TO_NUMBER .',
                "message_type": "text",
                "text":'. $message.',
                "channel": "whatsapp"}\''
            ;*/

            //dump($content);

          /* $content= 'curl -X POST https://messages-sandbox.nexmo.com/v1/messages \
            -u '.$user.' \
            -H '.$header1.' \
            -H '.$header2.' \
            -d \''.'{
                "from": "14157386102",
                "to":'. $TO_NUMBER.',
                "message_type": "text",
                "text":'. $message.',
                "channel": "whatsapp"
            }\'';

            shell_exec($content);*/
            /*
            $response = $client->sms()->send(
                new \Vonage\SMS\Message\SMS("33769221935",'VONAGE TEST API', 'A text message sent using the Nexmo SMS API')
            );

            $message = $response->current();

            if ($message->getStatus() == 0) {
                echo "The message was sent successfully\n";
            } else {
                echo "The message failed with status: " . $message->getStatus() . "\n";
            }*/

        }

    }

}
