<?php

namespace App\Listeners;

use App\Event\OffrandeDimeModify;
use App\Http\Controllers\MailController;
use App\Models\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendEmailOffrandeDimeModify
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Event\OffrandeDimeModify  $event
     * @return void
     */
    public function handle(OffrandeDimeModify $event)
    {

        $users = new User();
        $mail = new MailController;

        $emails=$users->getEmailUserPasteurAndRespo();
        //dd($event);
        $subject = '📢 Modification Offrande-Dîme';
        $content = 'Bonjour, modification depense n°'.$event->offrandeDime['Id'].' d\'un nouveau montant de '.$event->offrandeDime['Montant'].' € le '.$event->offrandeDime['Date'].'👍<br>.';
        $content .='Montant précédent '.$event->offrandeDime['Montant_prev']. ' € <br/>';
        foreach ($emails as $key => $value) {

            $mail->sendEmailFidele($value, $subject, $content);
        }
    }
}
