<?php

namespace App\Listeners;

use App\Event\OffrandeDimeDepenseSuppression;
use App\Http\Controllers\MailController;
use App\Jobs\SendMailJob;
use App\Models\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendEmailNotificationOffrandeDimeDepense
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Event\OffrandeDimeDepenseSuppression  $event
     * @return void
     */
    public function handle(OffrandeDimeDepenseSuppression $event)
    {
        //SendMailJob::dispatchAfterResponse($event);
        $user = new User();
        $mail = new MailController;
        $emails=$user->getEmailUserPasteurAndRespo();

        $subject = '📢 Confirmation suppression '.$event->offrandeDimeDepense['Libelle'].' n°: '.$event->offrandeDimeDepense['Id'];

        $content = '️🚨 Bonjour suppression '.$event->offrandeDimeDepense['Libelle'].' n°: '.$event->offrandeDimeDepense['Id']. ' d\'une valeur  de ' .$event->offrandeDimeDepense['Montant'].' € le '.$event->offrandeDimeDepense['Date'].' par : '.$event->offrandeDimeDepense['Remove_by'].' <br/> Merci.';

        foreach ($emails as $key => $value) {

            $mail->sendEmailFidele($value, $subject, $content);
        }


    }
}
