<?php

namespace App\Listeners;

use App\Event\DepenseCreated;
use App\Http\Controllers\MailController;
use App\Models\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendEmailDepenseCreated
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Event\DepenseCreated  $event
     * @return void
     */
    public function handle(DepenseCreated $event)
    {
        $users = new User();
        $mail = new MailController;

        $emails=$users->getEmailUserPasteurAndRespo();
        $subject = '📢 Enregistrement Depenses';
        $content = 'Bonjour, enregistrement depense n°'.$event->depense['Id'].
        ' d\'un  montant de '.$event->depense['Montant'].' € le '.$event->depense['Date'].', 👍 <br>.';

        foreach ($emails as $key => $value) {

            $mail->sendEmailFidele($value, $subject, $content);
        }



    }
}
