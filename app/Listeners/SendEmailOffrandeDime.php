<?php

namespace App\Listeners;

use App\Event\OffrandeDimeCreated;
use App\Http\Controllers\MailController;
use App\Models\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendEmailOffrandeDime
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Event\OffrandeDimeCreated  $event
     * @return void
     */
    public function handle(OffrandeDimeCreated $event)
    {
        $users = new User();
        $mail = new MailController();

        $emails=$users->getEmailUserPasteurAndRespo();

        $subject = '📢 Enregistrement Offrandes';
        $content = 'Bonjour, enregistrement offrande n°'.$event->offrandeDime['Id'].' d\'un montant de '.$event->offrandeDime['Montant'].' € le '.$event->offrandeDime['Date'].'👍<br>.';

        foreach ($emails as $key => $value) {

            $mail->sendEmailFidele($value, $subject, $content);
        }


    }


}
