<?php

namespace App\Notifications;

use Http;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Monolog\Handler\SlackWebhookHandler;

class SendNotification extends Notification
{
    use Queueable;
    private $user;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        $this->user = $user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        //return ['mail'];
        return ['slack'];
    }

    public function toSlack($notifiable)
   {

        $url='https://hooks.slack.com/services/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';
        $content='curl -X POST --data-urlencode "payload={\"channel\": \"#alertmeisatd\", \"username\": \"webhookbot\", \"text\": \"Ceci est publié dans #alertmeisatd et provient d\'un robot nommé webhookbot.\", \"icon_emoji\": \":ghost:\"}" '.$url;

        $reponse= Http::post($url,[
            'payload'=>[
                'channel'=>'#alertmeisatd',
                'username'=>'Meistad-Lyon',
                'text'=>'Welcome meistad',
                'icon_emoji'=>':ghost:'
            ]

        ]);
        dd($reponse);
       // shell_exec($content);
   }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }


}
