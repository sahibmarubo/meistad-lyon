<?php

namespace App\Providers;

use App;
use DB;
use Illuminate\Database\Connection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\ServiceProvider;
use Opcodes\LogViewer\Facades\LogViewer;
use Illuminate\Database\Events\QueryExecuted;
use PgSql\Connection as PgSqlConnection;
use URL;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->isLocal()) {
            $this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
        }
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        LogViewer::auth(function ($request) {

            return $request->user()
                && in_array($request->user()->email, [
                     'sahibmarubo@gmail.com',
                ]);
        });
        
        //use https in url 
        App::environment() == 'local' ? URL::forceScheme('http') : URL::forceScheme('https');
       
        //prevent forget attribites model
        Model::shouldBeStrict();

        //Custom Polymorphic Types
        Relation::enforceMorphMap([
             'user'=>'App\Models\User',
             'role'=>'App\Models\Role'
 
        ]);

    }
}
