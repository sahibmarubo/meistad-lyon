<?php

namespace App\Providers;

use App\Http\Controllers\MailController;
use Illuminate\Auth\Notifications\VerifyEmail;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Notifications\Messages\MailMessage;
use Laravel\Telescope\Http\Controllers\MailHtmlController;
use Illuminate\Support\Str;
class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();




        VerifyEmail::toMailUsing(function ($notifiable, $url) {
           
            //$needle="'/email/verify/'";
            //$segments = Str::of($url)->split($needle);

            //$url = $_ENV['APP_DEBUG']=="true"?$url:"https://meistad.ngrok.io/email/verify/".$segments[1];

            //$url="http://meistad.ngrok.io/email/verify".$keywords[1];
            $mail= new MailController();
            $ubject = '💂 Vérification adresse e-mail 💂';
            $content='<b>Bonjour pour valider votre adresse e-mail,
             merci de clicker sur le lien en desssous ⬇️ </b><br>';
            $content.="<a href=$url class='btn btn-success'><em>Verify Email Adress </em></a>";

            $mail->sendEmailFidele($notifiable['email'],$ubject,$content);
            
            return (new MailMessage)
                ->subject('Verify Email Address')
                ->line('Click the button below to verify your email address.')
                ->action('Verify Email Address', $url);
        });
    }
}
