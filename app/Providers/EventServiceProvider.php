<?php

namespace App\Providers;

use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        'App\Event\OffrandeDimeCreated' => [
            'App\Listeners\SendEmailOffrandeDime'
        ],
        'App\Event\OffrandeDimeModify' => [
            'App\Listeners\SendEmailOffrandeDimeModify'
        ],
        'App\Event\DepenseCreated' => [
            'App\Listeners\SendEmailDepenseCreated'
        ],
        'App\Event\DepenseModify' => [
            'App\Listeners\SendEmailDepenseModify'
        ],
        'App\Event\OffrandeDimeDepenseSuppression' => [
            'App\Listeners\SendEmailNotificationOffrandeDimeDepense'
        ],
        'App\Events\SendMessage' => [
            'App\Listeners\SendMessagesNotification'
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
