<?php

namespace App\Jobs;

use App\Http\Controllers\MailController;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SendMailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    public $details;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct( $details)
    {
        $this->details = $details;
        $this->onQueue('emails');
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $mail = new MailController;
        $user = new User();
        $emails=$user->getEmailUserPasteurAndRespo();

        $subject = '📢 Confirmation suppression '.$this->details['Libelle'].' n°: '.$this->details['Id'];

        $content = '️🚨 Bonjour suppression '.$this->details['Libelle'].
        ' n°: '.$this->details['Id']. ' d\'une valeur  de ' .$this->details['Montant'].
        ' € le '.$this->details['Date'].' par : '.$this->details['Remove_by'].' <br/> Merci.';

        foreach ($emails as $key => $value) {

            $mail->sendEmailFidele($value, $subject, $content);
        }

    }
}
