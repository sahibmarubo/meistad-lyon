<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Interfaces\BilanAction;
use App\Models\Depenses;
use App\Models\Offrandes;
use App\Services\BilanService;
use Illuminate\Support\Facades\DB;

class BilanController implements BilanAction
{
    /**
     * get bilan
     */
    public function getBilan()
    {
        date_default_timezone_set('Europe/Paris');
        try {
            $offrandes  = Offrandes::all()->sum('montant');

            $depenses = Depenses::all()->sum('montant');

             //dd($offrandes, $depenses);
            return ['date' => date('Y-m-d H:i:s'), 'Offrandes' => round($offrandes,2), 'Depenses' => round($depenses,2),'Bilan'=>round(($offrandes-$depenses),2)];
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Calcule bilan annuel  de l'année
     *
     * @param int $year
     * @return $result
     */
    public function searchBilanByYear($year)
    {
        date_default_timezone_set('Europe/Paris');
        try {
            $offrandes = DB::table('offrandes')
                ->whereYear('date_offrandes', $year)
                ->sum('montant');

            $depenses = DB::table('depenses')
            ->whereYear('date_depenses', $year)
            ->sum('montant');
        } catch (\Exception $e) {
            return $e->getMessage();
        }

        return ['date' => date('Y-m-d H:i:s'), 'Offrandes' => round($offrandes,2), 'Depenses' => round($depenses,2)];

    }
    /**
     * listing bilan by year
     *
     * @param BilanService $bs
     * @return \Illuminate\Http\Response
     */
    public function statCaisse(BilanService $bs)
    {
        $result=$bs->statCaisse();
       
        return $result;
    }

    public function listYear()
    {
        $bs = new BilanService;
        $result= $bs->listYear();
        return $result;

    }


}
