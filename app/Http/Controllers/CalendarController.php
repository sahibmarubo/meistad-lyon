<?php

namespace App\Http\Controllers;

use App;
use Auth;
use Illuminate\Http\Request;

class CalendarController extends Controller
{
   
    /**
     * Summary of secretariatCalendar
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function secretariatCalendar()
    {
        $user= Auth::user();
        
        return view('calendar.secretariat',compact('user'));
    }
    /**
     * Summary of publicCalendar
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function publicCalendar()
    {
        return view('calendar.public');
    }

   
}
