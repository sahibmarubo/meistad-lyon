<?php

namespace App\Http\Controllers;

use App\Models\Role;
use App\Models\User;
use App\Notifications\SendNotification;
use Http;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redis;
use PhpParser\Node\Stmt\TryCatch;
use Spatie\Activitylog\Models\Activity;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth','verified']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        //persit in db regis
       // Redis::set('name', 'Marubo');
        //get value with is key name
        //dd(Redis::get('name'));
        //dd(auth()->user()['name']);
        //$userName=auth()->user()['name'];
        //$arrayUser = array('name' =>$user['name'],'email'=>$user['email'],'role'=>$user['roles']);
        //activity()->log('Look mum, I logged something');
        //$lastActivity = Activity::all()->last();
    
        return view('home');
    }

    /**
     * Get current User infos
     */
    public function currentuser()
    {
        $user = auth()->user();

        //dd(auth()->user());
        $arrayUser = ['name' => $user['name'], 'email' => $user['email'],
            'id_user' => $user['id'],
            'role_id' => $user['role_id'],'phone'=>$user['phone']];

        return response()->json($arrayUser);
    }

    /**
     * Get infos user by Id
     *
     */
    public function getUserById($id)
    {
        try {
            $user = User::whereId($id)->get(['name','email','phone'])->toArray();
        } catch (\Exception $e) {
            //throw $th;
            return $e->getMessage();
        }

        return $user;

    }

    public function getRoles()
    {
        try {
            $roles = Role::all(['display_name','id']);
        } catch (\Exception $e) {
            //throw $th;
            return $e->getMessage();
        }

        return $roles;
    }
}
