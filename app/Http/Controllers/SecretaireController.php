<?php

namespace App\Http\Controllers;

use App\Events\SendMessage;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Notifications\Messages\MailMessage;
use Lang;
use PhpParser\Node\Stmt\Switch_;

class SecretaireController extends Controller
{
    /**
     * send Info for all user chruch
     */
    public function sendMessageMeistad(Request $request)
    {

        request()->validate([
            'content' => ['required'],
        ]);
        if(isset($request['content']['expeditRole'])) {
            $user_role=$request['content']['expeditRole'];
            $user_name=$request['content']['expeditName'];
        }else {
           // $user_role=$home->currentuser()->getData()->role_id;
            //$user_name=$home->currentuser()->getData()->name;
        }

        $signature= $this->signatureEmail($user_role);

        if($signature=='Message non signé'){
            return 'Message non signé envoi impossible';
        }else {
            $body= (new MailMessage)
            ->subject(Lang::get($request['content']['subject'].' : '.$signature))
            ->greeting(Lang::get('Bonjour'))
            ->line(Lang::get($request['content']['content']))
            ->line(Lang::get('Expéditeur: '.$user_name));

            $data=['Data'=>$body,'Desti'=>[],'type'=>$request['content']['type']];

            array_push($data['Data']->outroLines, 'Expéditeur: '.$user_name);

            //Check Destinataires specifiés
            if($request['content']['desti']) {
                //Envoi aux destinataires listes
                if($request['content']['type']=='EMAIL') {
                    $data['Desti']= $this->getEmail($request['content']['desti']);

                } elseif ($request['content']['type']=='SMS') {
                    $data['Desti']=$this->getPhone($request['content']['desti']);
                }

                // declenchement Event
                event(new SendMessage($data));

            } else {
                //envoi à tous les fidèles

                event(new SendMessage($data));

            }

            return 'Information envoyée avec succès';


        }


        /*
        $fideles = User::all('email');
        // dd($fideles);
        //  return $fideles;
        // dd(count($fideles));

        if (count($fideles) > 0) {
            for ($i = 0; $i < count($fideles); $i++) {
                $mail->sendEmailFidele($fideles[$i]['email'], $request['content']['subject'], $request['content']['content']);
            }


        } else {
            return 'Aucun fidele trouvés, envoi information impossible ';
        }*/

    }

    /**
     * send Info for a specific user
     */
    public function sendPersoUser(Request $request)
    {
        request()->validate([
            'content' => ['required'],
        ]);
        // return $request['content'];
        $mail = new MailController();

        $mail->sendEmailFidele($request['content']['mail'], $request['content']['subject'], $request['content']['content']);

        return 'Message envoyé avec success ';
    }

    /**
     * Send Info for a specific user
     */
    public function useremails()
    {
        $fideles = User::all(['email','name','phone']);

        return $fideles;
    }
    /**
     * Identification expediteur du message
     * @param int,string $id
     * @return string
     */
    public function signatureEmail(int|string $id)
    {

        switch ($id) {
            case 4:
                return  "Secrétaire";
                break;
            case 5:
                return "Pasteur";
                break;
            case 7:
                return "Comité Visite";
                break;
            default:
            return 'Message non signé';
        }

    }
    /**
     * Retourne liste de destinataires a recevoir messages
     *
     * @param array $data
     * @return array
     */
    public function getEmail($desti)
    {
        $data=[];
        foreach ($desti as $key => $value) {
            // echo "{$key} => {$value} ";
             $keywords = preg_split("/:/", $value);

             array_push($data,$keywords[1]);
        }

        return $data;
    }

    /**
     * Retourne liste de destinataires a recevoir messages
     *
     * @param array $desti
     * @return array
     */
    public function getPhone($desti) {
        $data=[];
        foreach ($desti as $key => $value) {
            // echo "{$key} => {$value} ";
             $keywords = preg_split("/:/", $value);
             $rest = substr($keywords[1], 1, 9);

             $number="33".$rest;
             array_push($data, $number);
        }
        return $data;

    }

}
