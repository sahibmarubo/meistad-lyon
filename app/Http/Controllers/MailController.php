<?php

namespace App\Http\Controllers;

use Mailjet\Client;
use Mailjet\Resources;

class MailController extends Controller
{
    private $api_key = 'eaa2f7ff295271e0d492dbfcf9f5f928';

    private $api_secret = '46471d656e90e63f48c3d615d9267abb';
    public $version= ['version' => 'v3.1'];



    /**
     * send email contact user
     */
    public function sendEmailContact($to_email, $subject, $content, $file)
    {
        $mj = new Client($this->api_key, $this->api_secret, true, ['version' => 'v3.1']);
        $jpgBase64 = base64_encode(file_get_contents($file));
        //$mj = new MailjetClient(getenv('MJ_APIKEY_PUBLIC'), getenv('MJ_APIKEY_PRIVATE'),true,['version' => 'v3.1']);
        //alixya09@gmail.com,"weddingalixmartial@gmail.com",
        //"Mail de confirmation de votre invitation au mariage du duo parfait"
        $body = [
            'Messages' => [
                [
                    'From' => [
                        'Email' => 'meistadlyon@gmail.com',
                        'Name' => 'MEISTAD-LYON',
                    ],
                    'To' => [
                        [
                            'Email' => $to_email,

                        ],
                    ],
                    'TemplateID' => 3118403,
                    'TemplateLanguage' => true,
                    'Subject' => $subject,
                    'Variables' => [
                        'content' => $content,
                        'Attachments' => [
                            [
                                'ContentType' => 'image/jpeg',
                                'Filename' => "depense.jpg",
                                'Base64Content' => $jpgBase64
                            ],
                        ],

                    ],

                ],
            ],
        ];
        $response = $mj->post(Resources::$Email, ['body' => $body]);

        $response->success();
        //dd($response->getData());
    }

    /**
     * send email infos all user
     */
    public function sendEmailFidele($to_email, $subject, $content)
    {
        $mj = new Client($this->api_key, $this->api_secret, true, ['version' => 'v3.1']);

        //$mj = new MailjetClient(getenv('MJ_APIKEY_PUBLIC'), getenv('MJ_APIKEY_PRIVATE'),true,['version' => 'v3.1']);
        //alixya09@gmail.com,"weddingalixmartial@gmail.com",
        //"Mail de confirmation de votre invitation au mariage du duo parfait"
        $body = [
            'Messages' => [
                [
                    'From' => [
                        'Email' => 'meistadlyon@gmail.com',
                        'Name' => 'MEISTAD-LYON',
                    ],
                    'To' => [
                        [
                            'Email' => $to_email,

                        ],
                    ],
                    'TemplateID' => 3118403,
                    'TemplateLanguage' => true,
                    'Subject' => $subject,
                    'Variables' => [
                        'content' => $content,

                    ],

                ],
            ],
        ];
        $response = $mj->post(Resources::$Email, ['body' => $body]);

        $response->success();
        //dd($response->getData());
        //$uccess='Un mail vous a été envoyé, merci.';

        //return view('user.alertMail')->with('message',$uccess);
    }


}
