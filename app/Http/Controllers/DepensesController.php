<?php

namespace App\Http\Controllers;

use App\Event\DepenseCreated;
use App\Event\DepenseModify;
use App\Event\OffrandeDimeDepenseSuppression;
use App\Http\Controllers\Interfaces\BilanAction;
use App\Http\Controllers\Interfaces\FileAction;
use App\Http\Controllers\Interfaces\NotifyAction;
use App\Jobs\SendMailJob;
use App\Models\Depenses;
use App\Models\FileUpload;
use App\Models\User;
use App\Services\DepenseService;
use App\Traits\ImageUpload;
use DateTime;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class DepensesController implements FileAction,NotifyAction,BilanAction
{
    use ImageUpload;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $depenses = Depenses::orderBy('id', 'DESC')->get();
             //dd (round($depenses->sum('montant'),2) );
            return ['Data'=>$depenses,'Total'=>round($depenses->sum('montant'),2)];
        } catch (\Exception $e) {
            activity()->log('Depense show failded');
            return $e->getMessage();
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        request()->validate(
            [
                'date' => ['required'],
                'montant'=>['required'],
                'user'=>['required']
            ]
        );

        //Save Justificatif and save offrande or modif
        $fileName=$this::upload($request);
        //saveFile($request);

        //Insertion new depense

        // fixed auto increment primary key

        $lastDepense = Depenses::orderBy('id','desc')->first();

        DB::statement('alter sequence depenses_id_seq restart with '.(intval( $lastDepense->id)+1));

        try {
            $dep = Depenses::create([

                'id_user' => $request['user'],
                'date_depenses' => $request['date'],
                'montant' => $request['montant'],
                'justificatif' => $fileName,
                'remarques' => $request['obs'],
            ]);

            //Declenchement Event Depense to send Email
            $data=['Id'=>$dep->id,'Date'=>$dep->date_depenses,'Montant'=>$dep->montant];
            event(new DepenseCreated($data));

            return 'Dépense enregistrée avec success';

        } catch (\Exception $e) {
            activity()->log('Depense create failded');
            return $e->getMessage();
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Depenses  $depenses
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Depenses  $depenses
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Depenses $depenses)
    {
        request()->validate([
            'id'=>['required'],
            'date' => ['required'],
            'montant'=>['required'],
            'user'=>['required']
        ]);


        $fileName=$this::upload($request);
        //->saveFile($request);

        try {
            $depense = Depenses::findorfail($request['id']);
            if($depense->id) {
                //Event listener Depense Update

                $data=['Id'=>$depense->id,'Date'=>$request['date'],'Montant'=>$request['montant'],'Montant_prev'=>$depense->montant];

                $depense->update([

                    'id_user' => $request['user'],
                    'date_depenses' => $request['date'],
                    'montant' => $request['montant'],
                    'justificatif' => $fileName,
                    'remarques' => $request['obs']?:$depense->obs,
                ]);

                event(new DepenseModify($data));

                return 'Dépense modifiée avec succès';

            }

        } catch (\Exception $e) {
            activity()->log('Depense update failded');
            return $e->getMessage();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Depenses  $depenses
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,$currentuser)
    {
        date_default_timezone_set('Europe/Paris');
        try {

            $depense = Depenses::findorfail($id);
            $currentuser = User::findorfail($currentuser);


            if ($depense) {

                // Persiste depenss avt suppression
                $reponse =$this->retourArriere($depense);

                if($this->retourArriere($depense)=='success') {

                    $date=Date('Y-m-d H:m:s');

                     // Suupression depenses
                     Depenses::destroy($id);


                    $data=['Id'=>$depense->id,'Date'=>$date,'Montant'=>$depense->montant,'Remove_by'=>$currentuser->name,'Libelle'=>'dépense'];
                     // Queue Mail
                    SendMailJob::dispatch($data);
                     // Declenchment Event
                    //event(new OffrandeDimeDepenseSuppression($data));

                    return 'Depense supprimée avec succès';
                }


            } else {
                return 'Suppression  impossible depense n° '.$id.' introuvable';
            }

        } catch (\Exception $e) {
            activity()->log('Depense destroy failded');
            return $e->getMessage();
        }
    }

    /**
     * get depense by id
     * @return \Illuminate\Http\Response
     */
    public function getDepenseByid($id)
    {
        $depense = new Depenses();
        $result = $depense->getDepenseByid($id);

        return  $result;
    }

    /**
     * Get list of depenses with year
     * @return \Illuminate\Http\Response
     */
    public function searchDepenseByYear($year)
    {
        $depense = new Depenses();
        $results = $depense->searchDepenseByYear($year);


        return  ['Total' =>round($results->sum('montant'),2), 'Data' => $results];
    }


    /**
     * Get list of depenses with year by month
     *
     * @param int $year
     * @param string $month
     * @return \Illuminate\Http\Response
     */
    public function searchDepenseByMonth($year, $month)
    {

        $depense = new Depenses();
        $results = $depense->searchDepenseByMonth($year, $month);

        return  ['Total' =>round($results->sum('montant'),2), 'Data' => $results];

    }
    
    /**
     * Save before destroy for rollback
     *
     * @param json $object
     *  @return string
     */
    public function retourArriere($object)
    {
        $fileUpload = new FileUpload();
        $removename = $fileUpload->name = uniqid().'-'.date('Y-m-d H:i:s');
        Storage::disk('public')->put('removeDepenses/'.$removename.'',$object);
        $this->notify();

        return 'success';

    }
    /**
     * Notify on mobile mobile via ntfy
     *
     * @return void
     */
    public function notify()
    {
        file_get_contents('https://ntfy.sh/meistad_alerts', false, stream_context_create([
            'http' => [
                'method' => 'POST',
                'header' =>
                "Content-Type: text/plain\r\n" .
                "Tags: 📢 Suppression Dépense",
                "Priority: 5",
                'content' => ' Backup success 👍'
            ]
        ]));
    }
   /**
    * Undocumented function
    *
    * @param int $year
    * @return void
    */
    public function searchBilanByYear($year)
    {

    }
    /**
     * Get distint  year of depense
     *
     * @return \Illuminate\Http\Response
     */
    public function listYear()
    {
        $pds = new DepenseService;
        $result=$pds->getListingYearDepense();
        return $result;


    }
}
