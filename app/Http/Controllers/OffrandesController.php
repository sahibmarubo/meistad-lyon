<?php

namespace App\Http\Controllers;

use App\Event\OffrandeDimeCreated;
use App\Event\OffrandeDimeDepenseSuppression;
use App\Event\OffrandeDimeModify;
use App\Http\Controllers\Interfaces\BilanAction;
use App\Http\Controllers\Interfaces\FileAction;
use App\Http\Controllers\Interfaces\NotifyAction;
use App\Http\Controllers\Interfaces\OffrandesDepenses;
use App\Jobs\SendMailJob;
use App\Models\FileUpload;
use App\Models\Offrandes;
use App\Models\User;
use App\Services\OffrandesDimesService;
use App\Traits\ImageUpload;
use DB;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Spatie\Image\Image;

class OffrandesController implements OffrandesDepenses,FileAction,NotifyAction,BilanAction
{
    use ImageUpload;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        try {
            $results = Offrandes::orderBy('id', 'DESC')->get();
        } catch (\Exception $e) {
            return $e->getMessage();
        }


        return ['Data'=>$results,'Total'=>round($results->sum('montant'),2)];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        //dd($request['content']['date']);

        //select user role pasteur
        $user = new User();

        $pasteur = $user->getEmailUserPasteurAndRespo();

        request()->validate(
            [
                'date' => ['required'],
                'montant'=>['required'],
                'user'=>['required']

            ]
        );

        //Save Justificatif and save offrande or modif
        
        $fileName=$this::upload($request);
    
        //->saveFile($request);

            if (isset($request['id'])) {
                // Etape Update Offrandes

                try {
                    $searchoffrandes = Offrandes::findorfail($request['id']);

                    $data=['Id'=>$searchoffrandes->id,'Date'=>$searchoffrandes->date_offrandes,'Montant'=>$searchoffrandes->montant];
                    event(new OffrandeDimeModify($data));

                    if (isset($searchoffrandes['id'])) {

                        $searchoffrandes->update([

                            'id_user' => $request['user'],
                            'date_depenses' => $request['date'],
                            'montant' => $request['montant'],
                            'justificatif' =>$fileName,
                            'remarques' => $request['obs'],
                        ]);

                        return 'Offrande_Dîme modifiée avec succès';
                    } else {
                        return 'Modification impossible, offrande n°'.$request['id'].' introuvable';
                    }
                } catch (\Exception $e) {
                    return $e->getMessage();
                }
            } else {
                //Insertion new offrandes

                //fixed auto increment primary key
                $lastOffrande = Offrandes::orderBy('id','desc')->first();
                DB::statement('alter sequence offrandes_id_seq restart with '.(intval( $lastOffrande->id)+1));

                try {

                    $od=Offrandes::create(
                        [
                            'id_user' => $request['user'],
                            'date_offrandes' => $request['date'],
                            'montant' => $request['montant'],
                            'justificatif'=> $fileName,
                            'remarques' => $request['obs'],
                       ]
                    );

                    //Declenchement Event Offrndeime to send Email
                    $data=['Id'=>$od->id,'Date'=>$od->date_offrandes,'Montant'=>$od->montant];
                    event(new OffrandeDimeCreated($data));

                    return 'Enregistrement offrande_dîme reussie avec succès';

                } catch (\Exception $e) {
                    return $e->getMessage();
                }
            }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Offrandes  $offrandes
     * @return \Illuminate\Http\Response
     */
    public function show(Offrandes $offrandes)
    {
        try {
            //dd($offrandes);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Offrandes  $offrandes
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Offrandes $offrandes)
    {


        request()->validate([
            'id'=>['required'],
            'date' => ['required'],
            'montant'=>['required'],
            'user'=>['required']
        ]);


        $fileName=$this::upload($request);
        //->saveFile($request);

        try {
            $offrandes = Offrandes::findorfail($request['id']);

            if (isset($offrandes->id)) {

                $data=['Id'=>$offrandes->id,'Date'=>$request['date'],'Montant'=>$request['montant'],'Montant_prev'=>$offrandes->montant];
                event(new OffrandeDimeModify($data));

                $offrandes->update([

                            'id_user' => $request['user'],
                            'date_offrandes' => $request['date'],
                            'montant' => $request['montant'],
                            'justificatif' =>$fileName,
                            'remarques' => $request['obs'],
                        ]);

                        return 'Offrande_Dîme modifiée avec succès';
            } else {
                return 'Modification impossible offrande-dîme introuvable';
            }
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Offrandes  $offrandes
     * @return \Illuminate\Http\Response
     */
    public function destroy( $id,$currentuser)
    {

        date_default_timezone_set('Europe/Paris');

        try {
                $searchoffrandes = Offrandes::findorfail($id);
                $currentuser = User::findorfail($currentuser);

                if (isset($searchoffrandes->id)) {

                    //Sauvegarde avt suppression
                    $ra=$this->retourArriere($searchoffrandes);
                    $date=Date('Y-m-d H:m:s');
                    //Suppression
                    if ($ra=='success') {

                        try {
                            Offrandes::destroy($id);
                        } catch (\Exception $e) {
                            //throw $th;
                            return $e->getMessage();
                        }


                        $data=['Id'=>$searchoffrandes->id,'Date'=>$date,'Montant'=>$searchoffrandes->montant,'Remove_by'=>$currentuser->name,'Libelle'=>'offrande/dîme'];
                         //Quueue mail
                        SendMailJob::dispatch($data)->onQueue('emails');
                        //Event declenchement
                        //event(new OffrandeDimeDepenseSuppression($data));



                        return 'Suppression reussie';

                    } else {
                        return 'Suppression impossible ';
                    }

                } else {
                    return 'Suppression impossible , offrande n°: '.$id.' introuvable';
                }
            } catch (\Exception $e) {
                return $e->getMessage();
            }

    }

    /**
     * Get offrandes by id
     */
    public function getOffrandesByid($id)
    {

        $offrande = new Offrandes();

        $result = $offrande->getOffrandesByid($id);

        return  $result;
    }

    /**
     * get offrandes by year
     */
    public function SearchOffrandesByYear($year)
    {
        //dd($year);
        $offrande = new Offrandes();
        $results = $offrande->SearchOffrandesByYear($year);

        return  $results;
    }
    /**
     *  Get offrandes-dimes by year
     *
     * @param int $year
     * @return void
     */
    public function searchDepsenseOfrandresByYear($year)
    {
        $path = env('PATH_OFFRANDES');

        $filename = $path.'offrandes.txt';
        $offrande = new Offrandes();
        $results = $offrande->SearchOffrandesByYear($year);


        return  ['Total' =>round($results->sum('montant'),2)  , 'Data' => $results];


    }
    /**
     * Display offrande-dime with month and a year
     *
     * @param int $year
     * @param int $month
     * @return array
     */
    public function searchDepsenseOfrandresByYearAndMonth($year, $month)
    {
        $path = env('PATH_OFFRANDES');

        $filename = $path.'offrandes.txt';
        $offrande = new Offrandes();

        $results = $offrande->searchOffrandesByMonth($year,$month);
        //dd($results);

        return  ['Total' =>round($results->sum('montant'),2)  , 'Data' => $results];

    }
    
    /**
     * Rollback offrandeDime remove
     *
     * @param json $object
     * @return string
     */
    public function retourArriere($object)
    {
        $fileUpload = new FileUpload();
        $removename = $fileUpload->name = uniqid().'-'.date('Y-m-d H:i:s');
        Storage::disk('public')->put('removeOffrandes/'.$removename.'', $object);
        $this->notify();
        return 'success';

    }
    /**
     *Notification on mobile via nfty
     * @return void
     */
    public function notify()
    {
        file_get_contents('https://ntfy.sh/meistad_alerts', false, stream_context_create([
            'http' => [
                'method' => 'POST',
                'header' =>
                "Content-Type: text/plain\r\n" .
                "Tags: 📢 Suppression offrande-dîme",
                "Priority: 5",
                'content' => ' Backup success 👍'
            ]
        ]));
    }
    /**
     * Undocumented function
     *
     * @param int $year
     * @return
     */
    public function searchBilanByYear($year)
    {

    }
    /**
     * Listing Year ofrandeDime
     *
     * @return void
     */
    public function listYear()
    {
        $od = new OffrandesDimesService;
        $results = $od->getListYearOffrandes();
        return $results;

    }
}
