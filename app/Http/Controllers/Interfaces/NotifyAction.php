<?php
namespace App\Http\Controllers\Interfaces;
use Illuminate\Http\Request;
interface NotifyAction
{
    /**
     * Notification sur mobile via ntfy.sh
     *
     * @return void
     */
    public function notify();
}
