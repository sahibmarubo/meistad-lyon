<?php

namespace App\Http\Controllers\Interfaces;
interface OffrandesDepenses
{
    public function searchDepsenseOfrandresByYear($year);
    public function searchDepsenseOfrandresByYearAndMonth($year, $month);

}


