<?php
namespace App\Http\Controllers\Interfaces;
interface BilanAction
{
    public function searchBilanByYear($year);

    public function listYear();
}
