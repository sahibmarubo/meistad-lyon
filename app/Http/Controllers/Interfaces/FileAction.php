<?php
namespace App\Http\Controllers\Interfaces;
use Illuminate\Http\Request;
interface FileAction
{
    /**
     * Persist infos before remov it in a public folder
     *
     * @return void
     */
    public function retourArriere($object);

}
