<?php

namespace App\Http\Controllers;

use App\Models\Depenses;
use App\Models\FileUpload;
use App\Models\Offrandes;
use Illuminate\Http\Request;
use App\Models\User;
use DB;

class FileUploadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\FileUpload  $fileUpload
     * @return \Illuminate\Http\Response
     */
    public function show(FileUpload $fileUpload)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\FileUpload  $fileUpload
     * @return \Illuminate\Http\Response
     */
    public function edit(FileUpload $fileUpload)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\FileUpload  $fileUpload
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FileUpload $fileUpload)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\FileUpload  $fileUpload
     * @return \Illuminate\Http\Response
     */
    public function destroy(FileUpload $fileUpload)
    {

    }

    public function upload(Request $request)
    {
        $request->validate([
            'file' => 'required|mimes:jpg,jpeg,png,csv,txt,xlx,xls,pdf|max:2048',
        ]);

        $fileUpload = new FileUpload;
          //dd($request->file());

        if ($request->file()) {
            // dd($request);
            $file_name = time().'_'.$request->file->getClientOriginalName();
            $file_path = $request->file('file')->storeAs('uploads', $file_name, 'public');

            $fileUpload->name = time().'_'.$request->file->getClientOriginalName();
            $fileUpload->path = '/storage/'.$file_path;

            // dd($fileUpload->path);

            $fileUpload->save();

            $mail = new MailController();
            // get emails for user pasteur and respo
            $user = new User();
            $emails=$user->getEmailUserPasteurAndRespo();

            //get current user
            try {
                $currentuser = User::whereId($request['user'])->get('email')->toArray();
            } catch (\Exception $e) {
                return $e->getMessage();
            }
            /*
            if (isset($currentuser[0]['email'])) {
                $currentUser =$currentuser[0]['email'];
            } else {
                $currentUser='sahibmarubo@gmail.com'; //user admin or user secretaire 'potenoell@gmail.com'
            }

            //listing user email
            array_push($emails, $currentUser);*/

            if ($request['model'] == 'meistad') {

                if ($request['action'] == 'ajouter') {

                    // fixed auto increment primary key

                    $lastDepense = Depenses::orderBy('id','desc')->first();
                    //dd( $lastDepense);

                    DB::statement('alter sequence depenses_id_seq restart with '.(intval( $lastDepense->id)+1));

                    try {
                        $dep = Depenses::create([

                            'id_user' => $request['user'],
                            'date_depenses' => $request['date'],
                            'montant' => $request['montant'],
                            'justificatif' => $fileUpload->name,
                            'remarques' => $request['obs'],
                        ]);

                        //send email to user pasteur , respo, admin
                        $notifiaction = 'Dépense enregistrée avec success';

                        $user = auth()->user(); //impossible d'avoir cuurent user dans fileupload
                        $user_email = 'meistadlyon@gmail.com';
                        $user_email2 = 'potenoell@gmail.com';
                        $subject = '📢 Enregistrement Depenses';
                        $content = 'Bonjour, enregistrement depense n°'.$dep->id.' d\'un montant de '.$request['montant'].' € le '.$request['date'].'👍 <br>.';

                        foreach ($emails as $key => $value) {

                            $mail->sendEmailContact($value, $subject, $content, $request->file('file'));
                        }

                        return  $notifiaction;

                    } catch (\Exception $e) {
                        return $e->getMessage();
                    }
                } elseif ($request['action'] == 'modifier') {

                    try {
                        $depense = Depenses::findorfail($request['id']);

                        if ($depense) {
                            $depense->update([

                                'id_user' => $request['user'],
                                'date_depenses' => $request['date'],
                                'montant' => $request['montant'],
                                'justificatif' => $fileUpload->name,
                                'remarques' => $request['obs'],
                            ]);
                            //send email to user pasteur , respo, admin

                            $notifiaction = 'Depense modifiée avec success';

                            $user_email = 'meistadlyon@gmail.com';
                            $user_email2 = 'sahibmartial@gmail.com';
                            $subject = '📢 Modification Depenses';
                            $content = 'Bonjour, modification depense n°'.$depense->id.' d\'un montant de '.$request['montant'].' € le '.$request['date'].'👍<br>.';
                            $content .='Montant précedent '.$depense->montant. ' € <br/>';

                            foreach ($emails as $key => $value) {

                                $mail->sendEmailContact($value, $subject, $content, $request->file('file'));
                            }

                            return 'Dépense modifiée avec succès';

                        } else {
                            return 'Modification imposible, depense '.$request['id'].' introuvable';
                        }

                    } catch (\Exception $e) {
                        return $e->getMessage();
                    }

                } elseif ($request['action'] == 'ajouterOD') {

                    //step offrandes

                    try {
                         // fixed auto increment primary key

                        $lastOffrande = Offrandes::orderBy('id','desc')->first();

                        DB::statement('alter sequence offrandes_id_seq restart with '.(intval( $lastOffrande->id)+1));

                        $od=Offrandes::create(
                            [
                                'id_user' => $request['user'],
                                'date_offrandes' => $request['date'],
                                'montant' => $request['montant'],
                                'justificatif'=>$fileUpload->name,
                                'remarques' => $request['obs'],
                           ]
                        );
                        //send email
                        $subject = '📢 Enregistrement Offrandes';
                        $content = 'Bonjour, enregistrement offrande n°'.$od->id.' d\'un montant de '.$request['montant'].' € le '.$request['date'].'👍<br>.';

                        foreach ($emails as $key => $value) {

                            $mail->sendEmailContact($value, $subject, $content, $request->file('file'));
                        }

                        return 'Enregistrement offrande_dîme reussie avec succès';

                    } catch (\Exception $e) {
                        return $e->getMessage();
                    }
                } elseif ($request['action'] == 'modifierOD') {
                     //step offrandes

                    //dd($request);
                    try {
                        $od= Offrandes::findorfail($request['id']);

                    } catch (\Exception $e) {
                        return $e->getMessage();
                    }

                    if($od){

                        try {
                            $od->update([
                             'id_user' => $request['user'],
                             'date_offrandes' => $request['date'],
                             'montant' => $request['montant'],
                             'justificatif'=>$fileUpload->name,
                             'remarques' => $request['obs'],

                            ]);
                        } catch (\Exception $e) {
                            return $e->getMessage();
                        }
                        //send Email
                        $subject = '📢 Modification Depenses';
                        $content = 'Bonjour, modification depense n°'.$od->id.' d\'un montant de '.$request['montant'].' € le '.$request['date'].'👍<br>.';
                        $content .='Montant précedent '.$od->montant. ' € <br/>';

                        foreach ($emails as $key => $value) {

                          //  $mail->sendEmailContact($value, $subject, $content, $request->file('file'));
                        }


                        return 'Offrande_Dîme modifiée avec succès';
                    }

                    return 'Modification imposible, offrande n°: '.$request['id'].' introuvable';

                }
            }
        }
    }
}
