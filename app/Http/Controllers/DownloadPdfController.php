<?php

namespace App\Http\Controllers;

use App\Models\Offrandes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use PDF;

class DownloadPdfController extends Controller
{
    // Generate PDF
    public function createPDF()
    {
        // retreive all records from db
        $data = Offrandes::all();
        // share data to view
        view()->share('employee', $data);
        $pdf = PDF::loadView('pdf_view', $data);

        // download PDF file with download method
        return $pdf->download('pdf_file.pdf');
    }

    /**
     * Download file depenses
     */
    public function downlodFile(Request $request)
    {
        dd($request['content']);

        $filepath = public_path('download.csv');

        $pdf = PDF::loadView('pdf.pdf_view');
        dd($pdf);
        $filename = 'download.csv';
        //return $pdf->download('pdf_file.pdf');
        $headers = [
            'content-Type: pdf',
            'content-Disposition: attachment; filename=', $filename,
        ];

        return $pdf->download($request['content']);
    }

    /**
     * download iamge
     */
    public function getImage($avatar)
    {
        //echo asset('storage/file.txt');
        //return Storage::download(asset('storage/uploads/'.$avatar));
        return asset('storage/uploads/'.$avatar);
    }
}
