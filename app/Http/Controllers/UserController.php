<?php

namespace App\Http\Controllers;

use App\Models\User;
use Cookie;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Redirect;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        dd('Hello');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        dd($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $user = User::whereId($id)->get(['name','email','phone'])->first();
        } catch (\Exception $e) {
            //throw $th;
            return $e->getMessage();
        }

        return $user;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        if($request->request){
            //search User
            try {
                $user = User::findorfail($request->request->get('userid'));

                if($user) {
                    //verification same Email
                    if($request->request->get('email') == $user->email) {
                        $user->update([
                            'phone'=>$request->request->get('phone')
                        ]);

                    } else {
                        //Verification new email is unique
                        $verifNewEmail=User::whereEmail($request->request->get('email'))->first();
                        if($verifNewEmail){
                            return 'Modification Infos personnelle impossible';
                        }
                        //update user infos
                        $user->update([
                            'email'=>$request->request->get('email'),
                            'phone'=>$request->request->get('phone')
                        ]);
                    }
                    return response('Modification réussie');
                }

            } catch (\Exception $e) {
                return $e->getMessage();
            }

        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
     /**
      * Modification User password
      * @return void
      */
    public function modifUserPassword(Request $request)
    {

        if ($request->all()) {

            try {
                $user = User::whereEmail($request->all()['Email'])->get();
            } catch (\Exception $e) {
                return $e->getMessage();
            }
            //dd($user);
            if ($user->isNotEmpty()) {

                if (Hash::check($request->all()['OldPassword'], $user[0]['password'])) {

                     //change password
                    $request->validate([
                        'NewPassword' => ['required', 'string', 'min:8'],
                    ]);

                    $newPass= Hash::make($request->all()['NewPassword']);
                    $myuser=User::findorfail($user[0]['id']);
                    //dd($myuser);
                    try {
                        $myuser->update([
                            'password'=>$newPass
                        ]);
                    } catch (\Exception $e) {
                        //throw $th;
                        return $e->getMessage();
                    }

                    return response('Votre mot de passe a été mis à jour, merci!');

                }

                return response('Password pas identique, modification impossible');

            }

            return response('User not  found');

        }

        return response('Parameètre manquant');

    }
     /**
      * Send Link to reset user password
      *
      * @return void
      */
    public function SendEmailPassword(Request $request)
    {
        $mail = new MailController();
        $subject="Password oublié";
        $content="Une personne a demandé à changer votre mot de passe .<br>";
        $content.="Si vous êtes à l'origine de cette action  merci de bien cliquer sur ce lien suivant<br/>";
        //$content.=url('<a href="/password/reset">Click here</a>');
        $content.='<a style="color:red" href="http://localhost:8005/resetPassword">Click here</a>';

       //dd($request->all());
       $mail->sendEmailFidele($request->all()['email'], $subject, $content);


    }

    /**
     * Show form to reset user Password
     *
     * @param Request $request
     * @return void
     */
    public function getResetPasswordForm(Request $request)
    {
        //dd($request->route('cookie'));
        $cookie=$request->route('cookie');

        return view('user.resetPassword',compact('cookie'));

    }

    public function resetUserPassword(Request $request, $cookie)
    {
        // check valide time
         if($request->route('cookie') >= $_SERVER['REQUEST_TIME']) {
            Validator::make($request->all(), [

                'email' => ['required', 'string', 'email', 'max:255'],
               // 'password' => ['required', 'string', 'min:8'],
                'password_confirmation' => ['required_with:password|same:password|min:8']
            ]);

            //change password, search user
            try {
                $user = User::whereEmail($request->all()['email'])->get('id');

            } catch (\Exception $e) {
               //throw $th;
                return $e->getMessage();
            }

            if ($user->isNotEmpty()) {
                //Hash password
                $newPass= Hash::make($request->all()['password']);

               $user[0]->update([
                'password'=>$newPass
               ]);

               return  redirect()->route('login');

            }

            return response('Sorry user not  found');

        } else {

            return  view('auth.passwords.email');
        }

    }

    /**
     * Alerte email send to reset password
     *
     * @return void
     */
    public function EmailPassword(Request $request)
    {

        if ($request-> all()['_token']) {

            //creation date expiration du reste password
            $minutes=120;
            $cookie = cookie('delai', 'value', $minutes);
            //dd($cookie->getExpiresTime());

            $mail = new MailController();
            $subject="Password oublié";
            $content="Une personne a demandé à changer votre mot de passe .<br>";
            $content.="Si vous êtes à l'origine de cette action  merci de bien cliquer sur ce lien suivant. <br/>";
            $content.=" <strong>Lien valable 2h </strong> <br/>";
            $url="http://meistad.ngrok.io/resetPasswordForm/".$cookie->getExpiresTime()."";

            $content.='<a href="'.$url.'">Click here</a>';

            //check user mail before send email
            try {
                $verifemail= User::whereEmail($request->all()['email'])->get();
            } catch (\Exception $e) {
                return $e->getMessage();
            }

            if ($verifemail->isNotEmpty()) {
                $mail->sendEmailFidele($request->all()['email'], $subject, $content);
                 $uccess='Un mail vous a été envoyé, merci.';

            } else {
                $uccess='Email incorrect, merci';
            }

            return view('user.alertMail')->with('message',$uccess);
        }


    }

}
