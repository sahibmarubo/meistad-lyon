<?php

use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Auth::routes(['verify' => true]);
Auth::routes();
//USer

Route::get('/currentuser', [App\Http\Controllers\HomeController::class, 'currentuser']);
Route::post('passwordEmail',[App\Http\Controllers\UserController::class, 'SendEmailPassword'])->name('EmailPassword');
Route::post('/EmailPassword', [App\Http\Controllers\UserController::class, 'EmailPassword'])->name('EmailPassword');
Route::get('/resetPasswordForm/{cookie}', [App\Http\Controllers\UserController::class, 'getResetPasswordForm'])->name('resetpasswordForm');
Route::post('/resetPassword/{cookie}', [App\Http\Controllers\UserController::class, 'resetUserPassword'])->name('resetpassword');


Route::post('downloadfile', [\App\Http\Controllers\DownloadPdfController::class, 'downlodFile'])->name('downloadfile');


Route::middleware(['auth','verified'])->group(function () {

    Route::get('/roles', [App\Http\Controllers\HomeController::class, 'getRoles']);
   //Mailing
    Route::post('sendinfos', [\App\Http\Controllers\SecretaireController::class, 'sendMessageMeistad'])->name('sendinfos');
    Route::get('usersmails', [\App\Http\Controllers\SecretaireController::class, 'useremails'])->name('usersmails');
    Route::post('sendpersomessage', [\App\Http\Controllers\SecretaireController::class, 'sendPersoUser'])->name('sendpersomessage');

    //Bilan
    Route::get('bilan', [\App\Http\Controllers\BilanController::class, 'getBilan'])->name('bilan');
    Route::get('searchBilanByYear/{year}', [\App\Http\Controllers\BilanController::class,'searchBilanByYear'])->name('bilanYear');
    Route::get('caisse-stat',[App\Http\Controllers\BilanController::class,'statCaisse'])->name('StatCaisse');
    Route::get('/listing-year',[App\Http\Controllers\BilanController::class,'listYear'])->name('ListingYear');
    //Depenses
    Route::get('listingdepenses', [App\Http\Controllers\DepensesController::class, 'index'])->name('listingdepenses');

    Route::get('depense/{id}', [\App\Http\Controllers\DepensesController::class, 'getDepenseByid'])->name('getdepense');
    Route::delete('removedepense/{id}/{user}', [\App\Http\Controllers\DepensesController::class, 'destroy'])->name('removedepense');
    Route::get('downloadImage/{avatar}', [\App\Http\Controllers\DownloadPdfController::class, 'getImage'])->name('downloadImage');
    Route::get('searchDepByYear/{year}', [\App\Http\Controllers\DepensesController::class, 'searchDepenseByYear'])->name('searchdepbyyear');
    Route::get('searchDepByYearMonth/{year}/{month}', [\App\Http\Controllers\DepensesController::class, 'searchDepenseByMonth'])->name('searchdepbyyearmonth');
    Route::get('listing-year-depense',[App\Http\Controllers\DepensesController::class,'listYear'])->name('ListingYearDepense');

    Route::post('/depense_store',[App\Http\Controllers\DepensesController::class,'store'])->name('Save Depense');
    Route::post('/depense_update', [App\Http\Controllers\DepensesController::class,'update'])->name('Modification Depense');
    //Offrandes
    Route::get('/offrandes', [App\Http\Controllers\OffrandesController::class, 'index'])->name('listing.offrandes');
    Route::get('searchOffrande/{year}', [App\Http\Controllers\OffrandesController::class, 'searchDepsenseOfrandresByYear'])->name('searchoffrande');
    Route::get('searchOffrandeYearMonth/{year}/{month}', [\App\Http\Controllers\OffrandesController::class, 'searchDepsenseOfrandresByYearAndMonth'])->name('searchdepbyyearmonth');
    Route::post('/upload', [App\Http\Controllers\FileUploadController::class, 'upload'])->name('upload');

    Route::post('/offrande_store', [App\Http\Controllers\OffrandesController::class, 'store'])->name('Save ofrandre');
    Route::post('/offrande_update', [App\Http\Controllers\OffrandesController::class, 'update'])->name('Modify ofrandre');
    Route::delete('/offrande/{id}/{user}', [App\Http\Controllers\OffrandesController::class, 'destroy'])->name('destroy_Offrandes');
    Route::get('/offrande/{id}', [App\Http\Controllers\OffrandesController::class, 'getOffrandesByid'])->name('get_Offrande');
    Route::get('/listing-year-offrandes',[App\Http\Controllers\OffrandesController::class,'listYear'])->name('Get Year Offrandes');

    //User
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
    Route::put('/updateuser/{id}', [App\Http\Controllers\UserController::class, 'update'])->name('update');
    Route::get('/show/{id}', [App\Http\Controllers\UserController::class, 'show'])->name('showuser');
    Route::post('/updatePassword', [App\Http\Controllers\UserController::class, 'modifUserPassword'])->name('updatepassword');
    //calendar
    Route::group(['prefix' => 'calendar'], function () {
        Route::get('/secretariat',[\App\Http\Controllers\CalendarController::class,'secretariatCalendar'])->middleware('secretaire.calendar')->name('calendar.secretariat');
    });

});
//Exemple subscription channel redis
Route::get('/publish', function () {
    Redis::publish('test-channel', json_encode([
        'meistad' => 'Test du channel mis en place pas redis '
    ]));
});

Route::get('/suscribe', function () {
    print(Redis::get('test-channel') ?: 'Key not found').PHP_EOL;
   /* Redis::publish('test-channel', json_encode([
        'meistad' => 'Test du channel mis en place pas redis '
    ]));*/
});

//Guest calendar 
Route::get('calendar/public',[\App\Http\Controllers\CalendarController::class,'publicCalendar'])->name('calendar.public');
