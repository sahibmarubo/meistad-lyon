<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();

});

Route::middleware('auth:api')->group( function () {

    Route::get('/login', [App\Http\Controllers\UserController::class, 'index'])->name('show.user');
    Route::post('/register', [App\Http\Controllers\UserController::class, 'store'])->name('show.register');

});



//Route::get('/offrandes', [App\Http\Controllers\OffrandesController::class, 'index'])->name('show.offrandes');
//Route::post('/offrandes', [App\Http\Controllers\OffrandesController::class, 'update'])->name('update.offrandes');


Route::get('/userActif', [App\Http\Controllers\HomeController::class, 'currentuser'])->name('get.user');
