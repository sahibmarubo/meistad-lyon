<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOffrandesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offrandes', function (Blueprint $table) {
            $table->id();
            $table->integer('id_user')->unsigned()->nullable()->default(null);
            $table->foreign('id_user')->references('id')->on('users')->onUpdate('cascade')->onDelete('set null');
            $table->date('date_offrandes');
            $table->float('montant');
            $table->text('remarques')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offrandes');
    }
}
