<?php

namespace Database\Seeders;

use DB;
use Event;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class EventSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('events')->insert([
            [
                'id' => Str::uuid(),
                'title' => 'Prière',
                'start' => '2024-03-08',
                'end' => null,
                'cible'=>'public'
            ],
            [
                'id' => Str::uuid(),
                'title' => 'Carting',
                'start' => '2024-03-21',
                'end' => null,
                'cible'=>'public'
            ],
            [
                'id' => Str::uuid(),
                'title' => 'Ecodim reunion',
                'start' => '2024-04-25',
                'end' => null,
                'cible'=>'moniteur'
            ],
        ]);
    }
}
