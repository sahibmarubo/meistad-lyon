<?php

namespace Tests\Feature;

use App\Models\User;
use Http;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class OffrandeDimeTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    public function test_offrandesDimes()
    {

        $this->withoutMiddleware();

        $response = $this->get('/offrandes');


       // dd( $response->getData());
        //$user = User::factory()->create();
        //$response = $this->actingAs($user)->withCredentials('tresor@tresor.com', 'ubuntu21@')->get('/api/offrandes');
        //dd($response);
        //$response = Http::withBasicAuth('tresor@tresor.com', 'ubuntu21@')->get('http://localhost:8000/api/offrandes');
        //dd($response->getStatusCode());
        //$response->successful();

        $this->assertEquals(200, $response->status());
    }



    public function test_offrandesDimes_lastElemnt()
    {
        $this->withoutMiddleware();

        $response = $this->get('/offrandes');
        //dd($response->getData('total')['Data'][0]);

        $myResult= [
            "id" => 33,
            "id_user" => 9,
            "date_offrandes" => "2023-01-30",
            "montant" => "385",
            "remarques" => "just test new route",
            "created_at" => "2023-01-30 ",
            "updated_at" => "2023-02-01 ",
            "justificatif" => '1675265046_soshjuillet.pdf'
        ];

        $this->assertEquals($myResult,$response->getData('total')['Data'][0]);

    }





}
