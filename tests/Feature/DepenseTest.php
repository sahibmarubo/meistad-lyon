<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class DepenseTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    public function test_depense()
    {

        $this->withoutMiddleware();

        $response = $this->get('/listingdepenses');
        //dd($response);

        $this->assertEquals(200, $response->status());
    }

    public function test_depense_last()
    {
        $this->withoutMiddleware();

        $response = $this->get('/listingdepenses');

        $myResult= [
            "id" => 23,
            "id_user" => 9,
            "date_depenses" => "2023-02-01",
            "montant" => "50",
            "justificatif" => "1675274123_tcl_01102022_au_31102022-1.pdf",
            "remarques" => "demo",
            "created_at" => "2023-02-01 ",
            "updated_at" => "2023-02-01 "
        ];

        $this->assertEquals($myResult,$response->getData('total')['Data'][0]);



    }
}
