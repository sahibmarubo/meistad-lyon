<?php

namespace Tests\Feature;

use App\Events\SendMessage;
use App\Http\Controllers\SecretaireController;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Request as HttpRequest;
use Illuminate\Notifications\Messages\MailMessage;
use Lang;
use Request;
use Tests\TestCase;

class EnvoiMessageTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }
    /**
     * Envoi Mail message
     *
     * @param Request $request
     * @return void
     */
    public function test_envoiSms()
    {
        $secre = new SecretaireController;


        $request = new \Illuminate\Http\Request();
        $request->request->add(['subject' => 'Meistad']);
        $request->request->add(['content' => 'Meistad Alerte comité visite']);
        $request->request->add(['expeditRole' => 7]);
        $request->request->add(['expeditName' => 'Ouattara']);
        $request->request->add(['type' => 'EMAIL']);

        $signature=$secre->signatureEmail($request->request->get('expeditRole'));


        $body= (new MailMessage)
        ->subject(Lang::get($request->request->get('subject').' : '.$signature))
        ->greeting(Lang::get('Bonjour'))
        ->line(Lang::get($request->request->get('content')));
        //->line(Lang::get('Expéditeur: '.$request->request->get('expeditName')));


        $data=['Data'=>$body,'Desti'=>[],'type'=>$request->request->get('type')];
        array_push($data['Data']->outroLines, 'Expéditeur: '.$request->request->get('expeditName'));

        //$rep =event(new SendMessage($data));
        //dd( $rep);
        $response = $this->get('/');
        $response->assertStatus(200);
    }
}
