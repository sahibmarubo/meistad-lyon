@extends('layouts.app')

@section('content')

<body>
 
  @livewire('counter')  
  
  @stack('scripts')
   
</body>

@endsection
