@extends('layouts.app')

@section('content')
<body>
    @if (session()->has('error'))
        <div class="{{$alertMessage}}">
            {{ session('error') }}
        </div>
   @endif
    @livewire('calendar', ['user' => $user], key($user->id))

    @stack('scripts')
</body>

@endsection
