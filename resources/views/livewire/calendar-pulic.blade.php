<div>
    <div class="container" id='calendar-container' wire:ignore>
        <div id='calendar'></div>
    </div>
</div>
@assets
<script src='https://cdn.jsdelivr.net/npm/fullcalendar@6.1.15/index.global.min.js'></script>
<link href='https://cdn.jsdelivr.net/npm/fullcalendar@5.6.0/main.min.css' rel='stylesheet' />
@endassets
@script

<script>
    document.addEventListener('livewire:initialized', function () {
        const Calendar = FullCalendar.Calendar;
        const calendarEl = document.getElementById('calendar');
        const calendar = new Calendar(calendarEl, {
            headerToolbar: {
                left: 'prev,next today',
                center: 'title',
                right: 'dayGridMonth,timeGridWeek,timeGridDay,listWeek'

            },
            locale: 'fr',
            events: JSON.parse(@this.events),

        });

        calendar.render();
    });
</script>


<style>
    #calendar-container {
        display: grid;
        grid-template-columns: 200px 1fr;
        padding: 20px;
    }
  
    #calendar {
        grid-column: 2;
        height: 700px;
    }
    
</style>

@endscript