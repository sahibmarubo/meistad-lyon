import Accueil from './components/ExampleComponent';
import Offrandes from './components/Culte/ajouter';
import ListingOD from './components/Culte/listing';
import EditOD from './components/Culte/editer';
import RemoveOD from './components/Culte/supprimer';
import Depenses from './components/Depenses/ajouter';
import ListingDepenses from './components/Depenses/listing';
import  EditDepense from './components/Depenses/editer';
import  SupDepense from './components/Depenses/supprimer';
import Bilan from './components/Bilan/bilan';
import MeistadBilan from './components/Admin/BilanMeistad';
import Couture from './components/AccueilCouture';
import AddCouture from './components/Couture/ajouter';
import FormSecretaire from './components/Secretaire/form';
import FormMessageperso from './components/Secretaire/messagePerson';
import SearchDepenses from './components/Depenses/search';
import Dashboard from './components/AccueilMeistad';
import MonCompte from './components/users/monCompte';
import Editinfos from './components/users/edit.vue';
import ModifPasword from './components/users/updatePassword.vue';
import Crud from './components/users/crud.vue';
import formSMS from './components/Secretaire/formSms.vue';

import ChartJsBilan from './components/Bilan/chartjs.vue';
export default [
    {path: '/',component: Accueil},
     // Dashbaord tresorerie
    {path: '/dashboard/:role', name: 'dash_tresorerie',component: Dashboard, props:true},
    //User compte
    {path: '/moncompte', component: MonCompte},
    {path: '/edit_user/:id', name:'editUser', component: Editinfos, props:true},
    {path: '/password_user/:email', name: 'passwordUser', component: ModifPasword, props: true},
    {path: '/crud/:id', name: 'demo_prime', component: Crud, props:true},

    {path: '/couture/addcouture',component: AddCouture},

    {path: '/offrandes',component: Offrandes},
    {path: '/listingoffrandes',component: ListingOD},
    {path: '/editoffrandes/:id',name: 'offrandesedit',component: EditOD,props: true},
    {path: '/removeoffrandes/:id',name: 'offrandesremove',component: RemoveOD, props: true},

    {path: '/depenses',component: Depenses},
    {path: '/listingdepenses',component: ListingDepenses},
    {path: '/editdepenses/:id',name: 'editdepense', component: EditDepense,props: true},
    {path: '/removedepenses/:id',name: 'supdepense', component: SupDepense,props: true},
    {path: '/search',name: 'dashboardSearch', component: SearchDepenses,props: true},

    {path: '/bilan',name: 'bilan', component: Bilan,props: true},

    {path: '/bilanaddmin',name: 'bilanmeistad', component: MeistadBilan,props: true},
    {path: '/bilanStat',name: 'statBilan', component: ChartJsBilan,props: true},

    //Secretaire,comite visite
    {path: '/form',name: 'formsecret', component: FormSecretaire,props: true},
    {path: '/formmessageperso',name: 'formmessagepero', component: FormMessageperso,props: true},
    {path: '/sendSms',name: 'formSms', component: formSMS,props: true},


]
