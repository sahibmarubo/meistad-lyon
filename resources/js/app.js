/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue').default;

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('example-component', require('./components/ExampleComponent.vue').default);

import Vue from 'vue';
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
 import '@fortawesome/fontawesome-free/css/all.css'
 import '@fortawesome/fontawesome-free/js/all.js'
 import 'primevue/resources/themes/saga-blue/theme.css'; //theme
 import 'primevue/resources/primevue.min.css'; //core css
 import 'primeicons/primeicons.css';
 import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
 import PrimeVue from 'primevue/config';
 Vue.component('font-awesome-icon', FontAwesomeIcon)
 import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
 import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import Toolbar from 'primevue/toolbar';
Vue.use(BootstrapVue)
Vue.use(IconsPlugin)
 import FlashMessage from '@smartweb/vue-flash-message';
 Vue.use(FlashMessage);
 Vue.use(PrimeVue,Toolbar)

import Routes from './Routes'
import VueRouter from 'vue-router';

Vue.use(VueRouter)
const router = new VueRouter({
routes: Routes,
//mode: 'history'
})
const app = new Vue({
    el: '#app',

    router: router,

    components: {
  //      'example': Example
    }

});
